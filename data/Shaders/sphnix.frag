#version 330 core
uniform float time;
vec2 mouse;
uniform vec2 resolution;
in vec3 Position;
out vec4 color;

void main( void ) 
{
	mouse = vec2(0,0);
	vec2 uv = Position.xy/resolution.xy;
	uv = uv * 8. - 4.;
	
	float t = mod(floor(time*.5), 6.);
	
	bool p 			= true;
	float x 			= uv.x;
	if(t > 1. && mouse.y>.5) 	p	= fract(uv.x*.5)<.5;
	if(t>2.)		x	= fract(x)*2.;
	if(t>3.)		x 	*= 2.-x;
	if(t>4.)		x 	*= 1.-abs(1.-x)*.25;
	
	x =  p ? x : -x;
	
	
	float y		= float(x>uv.y);
	
	color 	= vec4(y);
}//sphinx