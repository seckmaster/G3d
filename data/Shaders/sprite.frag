#version 330 core
in vec2 TexCoords;
out vec4 color;

uniform sampler2D image;
uniform sampler2D depth;
uniform sampler2D bloomTex;
uniform float width, height, near, far;

uniform vec3 spriteColor;
uniform bool blending;
uniform float opacity;

// post processing effects
uniform bool blur;
uniform bool grayscale;
uniform bool kernel;
uniform bool edgedetection;
uniform bool inversion;
uniform bool ssao;
uniform bool bloom;

/**
* SSAO STUFF
*/
#define PI    3.14159265
int samples = 5; //samples on the first ring (4-8)
int rings = 3; //ring count (3-6)

vec2 rand(vec2 coord) //generating random noise
{
	float noiseX = (fract(sin(dot(coord ,vec2(12.9898,78.233))) * 43758.5453));
	float noiseY = (fract(sin(dot(coord ,vec2(12.9898,78.233)*2.0)) * 43758.5453));
	return vec2(noiseX,noiseY)*0.004;
}

float readDepth(vec2 coord) 
{
	return (2.0 * near) / (far + near - texture(depth, coord).x * (far - near)); 	
}

float compareDepths(float depth1, float depth2 )
{
	float aoCap = 1.0;
	float aoMultiplier = 100.0;
	float depthTolerance = 0.0000;
	float aorange = 60.0;// units in space the AO effect extends to (this gets divided by the camera far range
	float diff = sqrt(clamp(1.0-(depth1-depth2) / (aorange/(far-near)),0.0,1.0));
	float ao = min(aoCap,max(0.0,depth1-depth2-depthTolerance) * aoMultiplier) * diff;
	return ao;
}

void main()
{
	vec3 result = spriteColor * vec3(texture(image, TexCoords));

    if (kernel || blur || edgedetection) {
    	const float offset = 1.0 / 300.0;  
	    vec2 offsets[9] = vec2[](
	        vec2(-offset, offset),  // top-left
	        vec2(0.0f,    offset),  // top-center
	        vec2(offset,  offset),  // top-right
	        vec2(-offset, 0.0f),    // center-left
	        vec2(0.0f,    0.0f),    // center-center
	        vec2(offset,  0.0f),    // center-right
	        vec2(-offset, -offset), // bottom-left
	        vec2(0.0f,    -offset), // bottom-center
	        vec2(offset,  -offset)  // bottom-right    
	    );

	    vec3 sampleTex[9];
	    for(int i = 0; i < 9; i++)
	    {
	        sampleTex[i] = vec3(texture(image, TexCoords.st + offsets[i]));
	    }
		
		vec3 col = vec3(0.0f);
	    if (kernel) {
		    float kern[9] = float[](
		        -1, -1, -1,
		        -1,  9, -1,
		        -1, -1, -1
		    );
		    
		    for(int i = 0; i < 9; i++)
		        col += sampleTex[i] * kern[i];
	    }
	    else if (blur) {
	    	float kern[9] = float[](
			    1.0 / 16, 2.0 / 16, 1.0 / 16,
			    2.0 / 16, 4.0 / 16, 2.0 / 16,
			    1.0 / 16, 2.0 / 16, 1.0 / 16  
			);

		    for(int i = 0; i < 9; i++)
		        col += sampleTex[i] * kern[i];
	    }
	    else if (edgedetection) {
	    	float kern[9] = float[](
		        1, 1, 1,
		        1, 8, 1,
		        1, 1, 1 
			);

		    for(int i = 0; i < 9; i++)
		        col += sampleTex[i] * kern[i];
	    }
	    
	    result = col;
    }
	if (inversion)
    	result = vec3(1.0, 1.0, 1.0) - result;
    if (grayscale) {
    	float average = 0.2126 * result.r + 0.7152 * result.g + 0.0722 * result.b;
    	result = vec3(average, average, average);
    }
    if (ssao) {
		float depth = readDepth(TexCoords);
		float d;
		
		float aspect = width / height;
		vec2 noise = rand(TexCoords);
		
		float w = (1.0 / width)/clamp(depth,0.05,1.0)+(noise.x*(1.0-noise.x));
		float h = (1.0 / height)/clamp(depth,0.05,1.0)+(noise.y*(1.0-noise.y));
		
		float pw;
		float ph;

		float ao;	
		float s;
		float fade = 1.0;
		
		for (int i = 0 ; i < rings; i += 1)
		{
			fade *= 0.5;
			for (int j = 0 ; j < samples*i; j += 1)
			{
				float step = PI*2.0 / (samples*float(i));
				pw = (cos(float(j)*step)*float(i));
				ph = (sin(float(j)*step)*float(i))*aspect;
				d = readDepth(vec2(TexCoords.s+pw*w,TexCoords.t+ph*h));
				ao += compareDepths(depth,d)*fade;	
				s += 1.0*fade;
			}
		}
		
		ao /= s;
		ao = 1.0 - ao;
	    
		vec3 black = vec3(0.0,0.0,0.0);
		vec3 luminance;
		vec3 treshold = vec3(0.2,0.2,0.2);
		
		//luminance = clamp(max(black,luminance-treshold)+max(black,luminance-treshold)+max(black,luminance-treshold),0.0,1.0);
		luminance = vec3(1,1,1);
		
		result = vec3(result*mix(vec3(ao,ao,ao),vec3(1.0,1.0,1.0),luminance));
    }

	if (blending)
		color = vec4(1.0f, 1.0f, 1.0f, opacity) * vec4(result, 1.0f);
	else
		color = vec4(result, 1.0f);

	if (bloom) {
    	/*const float offset = 1.0 / 300.0;  
	    vec2 offsets[9] = vec2[](
	        vec2(-offset, offset),  // top-left
	        vec2(0.0f,    offset),  // top-center
	        vec2(offset,  offset),  // top-right
	        vec2(-offset, 0.0f),    // center-left
	        vec2(0.0f,    0.0f),    // center-center
	        vec2(offset,  0.0f),    // center-right
	        vec2(-offset, -offset), // bottom-left
	        vec2(0.0f,    -offset), // bottom-center
	        vec2(offset,  -offset)  // bottom-right    
	    );

	    vec3 sampleTex[9];
	    for(int i = 0; i < 9; i++)
	    {
	        sampleTex[i] = vec3(texture(depth, TexCoords.st + offsets[i]));
	    }
    	float kern[9] = float[](
		    1.0 / 16, 2.0 / 16, 1.0 / 16,
		    2.0 / 16, 4.0 / 16, 2.0 / 16,
		    1.0 / 16, 2.0 / 16, 1.0 / 16  
		);
		vec3 col = vec3(0.0f);
	    for(int i = 0; i < 9; i++)
	        col += sampleTex[i] * kern[i];*/



		color = color * vec4(result, 1.0f);
	}
}