#version 330 core
layout (location = 0) in vec4 vertex; // <vec2 position, vec2 texCoords>

out vec2 TexCoords;
out vec3 Position;

uniform mat4 model;
uniform mat4 projection;
uniform mat4 view;

uniform bool textureAnimation;
uniform float frameNumber;
uniform float numberOfFrames;

void main()
{
	if (!textureAnimation)
    	TexCoords = vertex.zw;
    else {
    	TexCoords.y = vertex.w;
    	if (vertex.x == 1.0f) {
    		float x = (frameNumber + 1.0) / numberOfFrames;
    		TexCoords.x = x;
    	}
    	else {
    		float x = frameNumber / numberOfFrames;
    		TexCoords.x = x;
    	}
    }
    gl_Position = projection * view * model * vec4(vertex.xy, 0.0, 1.0);
    Position = vec3(model * vec4(vertex.xy, 0.0f, 1.0f));
}