#version 330 core
in vec2 TexCoords;
out vec4 color;

uniform sampler2D image;

void main()
{
	vec2 texCoords = vec2(1.0 - TexCoords.x, TexCoords.y);

	float shadow = 0.0;
	vec2 texelSize = 1.0 / textureSize(image, 0);
	for(int x = -1; x <= 1; ++x)
	{
	    for(int y = -1; y <= 1; ++y)
	    {
	        float pcfDepth = texture(image, texCoords.xy + vec2(x, y) * texelSize).r; 
	        shadow += pcfDepth;        
	    }    
	}
	shadow /= 9.0;

	//float r = vec3(texture(image, texCoords)).r;
	//r = (r * 2.0 - 1.0);
	//color = vec4(r, r, r, 1.0 - r);
	color = vec4(0.0, 0.0, 0.0, 1-shadow);
}