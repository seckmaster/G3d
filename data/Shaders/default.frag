#version 330 core

layout (location = 0) out vec4 FragColor;
layout (location = 1) out vec4 BrightColor;  

uniform mat4 model;
in vec2 TexCoords;
in vec3 Normal;
in vec3 Position;
in vec4 FragPosLightSpace;
in mat3 TBN;

struct Material {
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
    float shininess;
};
uniform Material material;

struct Light {
    vec3 position;
    vec3 direction;
    int type;
    bool isEnabled;
  
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
	
	// point
    float constant;
    float linear;
    float quadratic;

    // spotlight
    float cutOff;
    float outerCutOff;
};

#define MAX_LIGHTS 10
uniform Light lights[MAX_LIGHTS]; 

uniform sampler2D texture_diffuse1;
uniform sampler2D texture_spec1;
uniform sampler2D texture_normal1;
uniform sampler2D depthMap;
uniform sampler2D shadowMap;

uniform bool normalMapping;
uniform bool parallaxMapping;

uniform vec3 viewPos;

out vec4 color;
float height_scale = 0.1f;

float ShadowCalculation(vec4 fragPosLightSpace)
{
    vec3 projCoords = fragPosLightSpace.xyz / fragPosLightSpace.w;
    projCoords = projCoords * 0.5 + 0.5;
    float closestDepth = texture(shadowMap, projCoords.xy).r;  
    float currentDepth = projCoords.z; 
    float shadow = currentDepth > closestDepth ? 1.0 : 0.0;  
    return shadow;
}

vec2 ParallaxMapping(vec2 texCoords, vec3 viewDir)
{ 
    float height =  texture(depthMap, texCoords).r;     
    return texCoords - viewDir.xy / viewDir.z * (height * height_scale);            
}

void main()
{    	
	vec3 norm;
	if (normalMapping) {
		norm = texture(texture_normal1, TexCoords).rgb;
    	norm = normalize(norm * 2.0 - 1.0);
	}
	else
    	norm = Normal;
    
	vec3 result = vec3(0.0f, 0.0f, 0.0f);

	for (int i = 0; i < MAX_LIGHTS; i++) {
		if (lights[i].isEnabled) {
			vec3 ambient;
			vec3 diffuse;
			vec3 specular;

		  	vec3 lightDir;
		    vec3 viewDir;
		    vec2 texCoords = TexCoords;

		    if (normalMapping) {
    		    vec3 TangentLightPos = TBN * lights[i].position;
			    vec3 TangentViewPos  = TBN * viewPos;
			    vec3 TangentFragPos  = TBN * Position;
		    	lightDir = normalize(TangentLightPos - TangentFragPos);
		    	viewDir = normalize(TangentViewPos - TangentFragPos);

		    	if (parallaxMapping) {
		    		texCoords = ParallaxMapping(TexCoords, viewDir);
		    		if(texCoords.x > 1.0 || texCoords.y > 1.0 || texCoords.x < 0.0 || texCoords.y < 0.0)
    					texCoords = TexCoords;
    			}
		    }
		    else {
			  	if (lights[i].type == 1)
			  		lightDir = normalize(-lights[i].direction);
			  	else
			    	lightDir = normalize(lights[i].position - Position);

		    	viewDir = normalize(viewPos - Position);
		    }

		    float diff = max(dot(norm, lightDir), 0.0);
			vec3 halfwayDir = normalize(lightDir + viewDir);

		    float spec = pow(max(dot(norm, halfwayDir), 0.0), material.shininess);

		    // Ambient
		    ambient = lights[i].ambient * vec3(texture(texture_spec1, texCoords)) * material.ambient;
		    // Diffuse 
		    diffuse = lights[i].diffuse * (diff) * vec3(texture(texture_diffuse1, texCoords)) * material.diffuse;
		    // Specular
		    specular = lights[i].specular * (spec) * vec3(texture(texture_spec1, texCoords)) * material.specular;

		    // point
		    if (lights[i].type == 0) {
			    // Attenuation
		        float distance    = length(lights[i].position - Position) / 100;
			    float attenuation = 1.0f / (lights[i].constant + lights[i].linear * distance + lights[i].quadratic * (distance * distance));    
				
			    ambient  *= attenuation; 
				diffuse  *= attenuation;
			    specular *= attenuation;
		    }
		    // directional
		    else if (lights[i].type == 1) {
		    }
		    // spotlight
		    else if (lights[i].type == 2) {
				// soft edges
		    	float theta = dot(lightDir, normalize(-lights[i].direction));    
			    float epsilon = (lights[i].cutOff - lights[i].outerCutOff);
			    float intensity = clamp((theta - lights[i].outerCutOff) / epsilon, 0.0, 1.0);

			    ambient  *= intensity;
			    diffuse  *= intensity;
			    specular *= intensity;

		        float distance    = length(lights[i].position - Position) / 100;
			    float attenuation = 1.0f / (lights[i].constant + lights[i].linear * distance + lights[i].quadratic * (distance * distance));    
			    //float attenuation = 1.0f / 0.0f;
			    //float attenuation = 1.0f / distance;
			    ambient  *= attenuation; 
			    diffuse  *= attenuation;
			    specular *= attenuation;
			}

			// Calculate shadow
		    float shadow = ShadowCalculation(FragPosLightSpace);
		    result += (ambient + /*(1.0 - shadow) **/ (diffuse + specular));
	   	}
	}
    
    FragColor = vec4(result, 1.0f);
    float brightness = dot(result, vec3(0.2126, 0.7152, 0.0722));
    if (brightness > 1.0)
    	BrightColor = vec4(result, 1.0f);
}