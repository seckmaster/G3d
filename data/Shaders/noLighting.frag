#version 330 core

in vec2 TexCoords;

struct Material {
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
    float shininess;
};
uniform Material material;

uniform sampler2D texture_diffuse1;
uniform sampler2D texture_spec1;
uniform sampler2D texture_normal1;

out vec4 color;

void main()
{ 
	// Ambient
    vec3 ambient = vec3(texture(texture_diffuse1, TexCoords)) * material.ambient;

    // Diffuse 
    vec3 diffuse = vec3(texture(texture_diffuse1, TexCoords)) * material.diffuse;
    
    // Specular
    vec3 specular = vec3(texture(texture_spec1, TexCoords)) * material.specular;

    color = vec4(ambient + diffuse + specular, 1.0f);
}