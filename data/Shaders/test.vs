#version 330 core
layout (location = 0) in vec4 vertex; // <vec2 position, vec2 texCoords>

out vec2 TexCoords;

uniform mat4 model;
uniform mat4 projection;
uniform mat4 view;

void main()
{
    TexCoords = vertex.zw;

    mat4 v = projection * view;
    float d = sqrt( view[0][0] * view[0][0] +
					view[1][0] * view[1][0] +
					view[2][0] * view[2][0]);
	v[0][0] = 1;
	v[1][1] = 1;
	v[2][2] = 1;


    gl_Position = v * model * vec4(vertex.xy, 0.0, 1.0);
}