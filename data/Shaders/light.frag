#version 330 core
out vec4 color;

struct Material {
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};
uniform Material mat;

void main()
{
    color = vec4(mat.ambient, 1.0f);
}