#version 330 core
layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 texCoords;
layout (location = 3) in vec3 tangent;
layout (location = 4) in vec3 bitangent; 

out vec2 TexCoords;
out vec3 Normal;
out vec3 Position;
out vec4 FragPosLightSpace;
out mat3 TBN;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform mat4 lightSpaceMatrix;

uniform bool normalMapping;

void main()
{
    gl_Position = projection * view * model * vec4(position, 1.0f);
    TexCoords = texCoords;
    FragPosLightSpace = lightSpaceMatrix * vec4(Position, 1.0);
    Position = vec3(model * vec4(position, 1.0f));

    mat3 normalMatrix = mat3(transpose(inverse(model)));
    if (normalMapping) {
	    //vec3 T = normalize(normalMatrix * tangent);
	    //vec3 B = normalize(normalMatrix * bitangent);
	    //vec3 N = normalize(normalMatrix * normal);
	   	//vec3 T = normalize(mat3(model) * tangent);
	    //vec3 B = normalize(mat3(model) * bitangent);
	    //vec3 N = normalize(mat3(model) * normal);
	    
	    vec3 T = normalize(vec3(model * vec4(tangent, 0.0)));
		vec3 N = normalize(vec3(model * vec4(normal, 0.0)));
		// re-orthogonalize T with respect to N
		T = normalize(T - dot(T, N) * N);
		// then retrieve perpendicular vector B with the cross product of T and N
		vec3 B = cross(T, N);
	    
	    TBN = (mat3(T, B, N));
    }
    else {
    	Normal = normalize(normalMatrix * normal);
    }
}