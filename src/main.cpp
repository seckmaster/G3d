#include "device/gDevice.h"
#include <algorithm>
#include <cstdio>
#include <ctime>
//#include <python2.7/Python.h>

using namespace std;
using namespace glm;

/**
 * WEAPONS STUFF
 */
class Weapon {
public:
    /**
     * Weapon attributes.
     */
    gSound *onFireSound;
    gSound *outOfAmmo;
    gRigidBody *weapon;
    vec3 size;
    vec3 rot;
    float fireRate; // bullets per second
    float lastShotS; // time when the gun was last fired

    mat4 projection;
    gSceneManager *mngr;
    gResourceManager *rmngr;

public:
    int ammo;

    Weapon(int ammo, float fireRate, const string &fireSound,
           const string &path, gSceneManager *mngr,
           gResourceManager *rmngr) :
            ammo(ammo), fireRate(fireRate), mngr(mngr), rmngr(rmngr) {
        onFireSound = mngr->createSound(fireSound, false, EST_AMBIENT);
        outOfAmmo = mngr->createSound("data/Sounds/noammo.wav", false,
                                      EST_AMBIENT);
        lastShotS = 0.0f;

//			projection = glm::ortho((float) -1280, (float) 1280, (float) -720,
//					(float) 720, -400.0f, 400.0f);
        projection = glm::perspective(45.0f, (float) 1280 / (float) 720,
                                      0.1f, 400.0f);

        weapon = new gRigidBody(new gAABB(vec3(), vec3()));
        weapon->setPosition(vec3(4, -9, 8));
        weapon->setSize(vec3(0.8, 0.8, 0.8));
        weapon->setShader(rmngr->loadShader("default.vs", "noLighting.frag", "weapon"));
        weapon->setRotation(-M_PI_2 + 0.2, 0, M_PI_2);
        weapon->setMaterial(gMaterial::CHROME);
        weapon->setModel(rmngr->load3dModel(path, "rocketl"));
        weapon->setProjection(projection);
        weapon->setView(mat4());
        mngr->weapon = weapon;
    }

    void onFire(const vec3 &pos, const vec3 &dir) {
        if (1 / fireRate > glfwGetTime() - lastShotS)
            return;

        lastShotS = glfwGetTime();
        if (ammo == 0)
            outOfAmmo->play();
        else {
            onFireSound->play();
            ammo--;

            gRigidBody *bullet = mngr->addRigidBody(pos, vec3(5, 5, 5),
                                                    vec3(15, 15, 15));
            bullet->setModel(rmngr->load3dModel("cube.obj", "cube"));
            bullet->setShader(rmngr->getShader("weapon"));
            bullet->setDiffuseTexture(0);
            bullet->setSpecularTexture(0);
            bullet->setMaterial(gMaterial::RUBY);
            string *data = new string("bullet");
            bullet->setUserData((void *) data);

            gPathAnimator *anim = mngr->createPathAnimator(LINE, 0.8);
            anim->addPoint(pos);
            anim->addPoint(pos + dir * 1000);
            anim->setLoop(false);
            anim->setDuration(1);
            anim->setOnStopEvent(EASE_STOP_ANIMATOR_AND_HIDE_OBJECT);

            bullet->addAnimator(anim);
        }

        cout << "Ammo: " << ammo << endl;
    }

    void render(const vec3 &playerPos) {
//			weapon->render();
//			weapon->update(1.0 / 60.0);
    }
};

/**
 * END OF WEAPONS STUFF
 */

/**
 * AI STUFF
 */
struct Bot {
    vector<int> path;
    gRigidBody *body;
    float velocity;
    bool agressive;

    Bot(gRigidBody *b, float v) :
            body(b), velocity(v), agressive(true) {
    }
};

class Maze {
public:
    struct Polygon {
        vec3 position;
        vector<int> neighbours;
    };

    Maze(gFPSCamera *player, gSceneManager *mngr, gResourceManager *rmngr,
         Weapon *weapon) {
        this->weapon = weapon;

        ifstream file("data/Models/Mapa/navigation.txt");
        if (file.is_open()) {
            string line;
            bool points = true;
            while (getline(file, line)) {
                // skip comments
                if (line.substr(0, 2) == "//")
                    continue;
                if (line == "neighbours") {
                    points = false;
                    continue;
                }

                if (points) {
                    int id;
                    float x, y, z;
                    sscanf(line.c_str(), "%d %f %f %f", &id, &x, &y, &z);
                    Polygon p;
                    p.position = vec3(x, y, z);
                    maze.push_back(p);
                } else {
                    int id;
                    int a, b, c, d;
                    sscanf(line.c_str(), "%d %d %d %d %d", &id, &a, &b, &c,
                           &d);
                    if (a != -1)
                        maze.at(id - 1).neighbours.push_back(a - 1);
                    if (b != -1)
                        maze.at(id - 1).neighbours.push_back(b - 1);
                    if (c != -1)
                        maze.at(id - 1).neighbours.push_back(c - 1);
                    if (d != -1)
                        maze.at(id - 1).neighbours.push_back(d - 1);
                }
            }
        } else
            cerr << "Navigation map doesnt exist!" << endl;

        pickupRespawnTimer = 15.0f;
        previousRespawn = 0.0f;
        this->player = player;

        /**
         * Create collision shapes.
         */
        mngr->addRigidBody(vec3(115, -100, 50), vec3(1), vec3(50, 25, 520));
        mngr->addRigidBody(vec3(-450, -100, 55), vec3(1), vec3(50, 25, 520));
        mngr->addRigidBody(vec3(-180, -100, 330), vec3(1), vec3(520, 25, 50));
        mngr->addRigidBody(vec3(-180, -100, -220), vec3(1), vec3(520, 25, 50));
        mngr->addRigidBody(vec3(-250, -100, -20), vec3(1), vec3(105, 25, 105));
        mngr->addRigidBody(vec3(-250, -100, 135), vec3(1), vec3(105, 25, 105));
        mngr->addRigidBody(vec3(-95, -100, 135), vec3(1), vec3(105, 25, 105));
        mngr->addRigidBody(vec3(-95, -100, -20), vec3(1), vec3(105, 25, 105));

        /**
         * Create two bots.
         */
        gRigidBody *body = mngr->addRigidBody(maze[0].position,
                                              vec3(3, 3, 3), vec3(10, 100, 10), 1);
        body->setModel(rmngr->getModel("nanosuit"));
        body->setShader(rmngr->getShader("default"));
        body->setMaterial(gMaterial::BRASS);
        string *str = new string("enemy");
        bots.push_back(new Bot(body, 0.75));
        body->setUserData(str);

        body = mngr->addRigidBody(maze[3].position, vec3(3, 3, 3),
                                  vec3(10, 50, 10), 1);
        body->setModel(rmngr->getModel("nanosuit"));
        body->setShader(rmngr->getShader("default"));
        body->setMaterial(gMaterial::BRASS);
        bots.push_back(new Bot(body, 0.75));
        str = new string("enemy");
        body->setUserData(str);

        rotAnim = mngr->createRotationAnimator(vec3(1, 1, 1),
                                               radians(vec3(0, 0, 100)));

        for (int i = 0; i < 9; i++) {
            gRigidBody *pickup = mngr->addRigidBody(
                    maze[i].position + vec3(0, 20, 0), vec3(0.5, 0.5, 0.5),
                    vec3(0, 0, 0), 0);
            pickup->setShader(rmngr->getShader("default"));
            pickup->addAnimator(rotAnim);
            pickup->setVisible(false);
            pickup->setRotation(vec3(-M_PI_2, 0, 0));
            pickUps.push_back(pickup);
            pickUpIds.push_back(-1);
        }

        ammoModel = rmngr->load3dModel("ammo/rocketam.md3", "rocketAmmo");
        armorModel = rmngr->load3dModel("armor/armor_red.md3", "armor");
        weaponModel = rmngr->getModel("rocketl");
        healthModel = rmngr->load3dModel("health/large_sphere.md3",
                                         "health");
        boostModel = rmngr->load3dModel("instant/quad.md3", "boost");

        respawnSound = mngr->createSound("data/Sounds/poweruprespawn.wav",
                                         false, EST_AMBIENT);

        boostPickupSound = mngr->createSound("data/Sounds/quaddamage.wav",
                                             false, EST_AMBIENT);
        armorPickupSound = mngr->createSound("data/Sounds/ar2_pkup.wav",
                                             false, EST_AMBIENT);
        weaponPickupSound = mngr->createSound("data/Sounds/w_pkup.wav",
                                              false, EST_AMBIENT);
        ammoPickupSound = mngr->createSound("data/Sounds/am_pkup.wav",
                                            false, EST_AMBIENT);
        heatlhPickupSound = mngr->createSound("data/Sounds/n_health.wav",
                                              false, EST_AMBIENT);
    }

    vector<int> solveMaze(const vec3 &src, const vec3 &dst) {
        int id_src = findClosestPoint(src);
        int id_dst = findClosestPoint(dst);

        if (id_src == id_dst)
            return vector<int>();

        vector<int> solution;

        vector<int> q;
        int dist[maze.size()];
        int prev[maze.size()];

        for (int i = 0; i < maze.size(); i++) {
            dist[i] = 999999;
            prev[i] = -1;
            q.push_back(i);
        }
        dist[id_src] = 0;

        while (!q.empty()) {
            int u = -1, minDist = 999999;
            for (int i : q) {
                if (minDist > dist[i]) {
                    u = i;
                    minDist = dist[i];
                }
            }

            q.erase(remove(q.begin(), q.end(), u), q.end());

            for (int n : maze[u].neighbours) {
                int alt = dist[u];
                if (alt < dist[n]) {
                    dist[n] = alt;
                    prev[n] = u;
                }
            }
        }

        solution.push_back(id_dst);
        while (prev[id_dst] != -1) {
            solution.push_back(prev[id_dst]);
            id_dst = prev[id_dst];
        }
        solution.pop_back();

        return solution;
    }

    void update() {
        if (glfwGetTime() - previousRespawn >= pickupRespawnTimer) {
            previousRespawn = glfwGetTime();
            int rnd = rand() % 9;
            int pickUp = rand() % 5;

            gRigidBody *b = pickUps[rnd];
            if (!b->isVisible()) {
                if (pickUp == 0) {
                    b->setModel(ammoModel);
                } else if (pickUp == 1) {
                    b->setModel(healthModel);
                } else if (pickUp == 2) {
                    b->setModel(armorModel);
                } else if (pickUp == 3) {
                    b->setModel(boostModel);
                } else if (pickUp == 4) {
                    b->setModel(weaponModel);
                }
                pickUpIds[rnd] = pickUp;
                b->setVisible(true);
                respawnSound->play();
            }
        }

        // check if player went over pickup item
        for (int i = 0; i < pickUps.size(); i++) {
            if (!pickUps[i]->isVisible())
                continue;

            if (length2(pickUps[i]->getPosition() - player->getPosition())
                <= 400.0f) {
                pickUps[i]->setVisible(false);
                if (pickUpIds[i] == 0) {
                    ammoPickupSound->play();
                    weapon->ammo += 15;
                }
                if (pickUpIds[i] == 1)
                    heatlhPickupSound->play();
                if (pickUpIds[i] == 2)
                    armorPickupSound->play();
                if (pickUpIds[i] == 3)
                    boostPickupSound->play();
                if (pickUpIds[i] == 4) {
                    weaponPickupSound->play();
                    weapon->ammo += 10;
                }
            }
        }

        for (vector<Bot *>::iterator it = bots.begin(); it != bots.end();
             ++it) {
            Bot *bot = (*it);
            if (!bot->body)
                return;

            if (bot->path.empty())
                bot->path = solveMaze(bot->body->getPosition(),
                                      player->getPosition());

            vec3 dst;
            if (bot->path.empty()) {
                // agressive bot goes directly at the player
                if (bot->agressive)
                    dst = player->getPosition();
                else
                    dst = maze[rand() % maze.size()].position;
            } else {
                dst = maze[bot->path.back()].position;
                if (length2(bot->body->getPosition() - dst) <= 20.0f)
                    bot->path.pop_back();
            }

            vec3 dir = normalize(dst - bot->body->getPosition())
                       * bot->velocity;
            dir.y = 0;
            bot->body->setPosition(bot->body->getPosition() + dir);
        }
    }

    vector<Polygon> maze;
    vector<Bot *> bots;
    vector<int> pickUpIds;
    vector<gRigidBody *> pickUps;
    float pickupRespawnTimer;
    float previousRespawn;
    gFPSCamera *player;
    gARotationAnimator *rotAnim;
    gSound *respawnSound;
    gSound *ammoPickupSound;
    gSound *weaponPickupSound;
    gSound *heatlhPickupSound;
    gSound *boostPickupSound;
    gSound *armorPickupSound;
    gModel3D *ammoModel;
    gModel3D *boostModel;
    gModel3D *healthModel;
    gModel3D *armorModel;
    gModel3D *weaponModel;
    Weapon *weapon;

public:
    int findClosestPoint(const vec3 &location) {
        float distance = distance2(location, maze[0].position);
        int closest = 0;

        for (int i = 1; i < maze.size(); i++) {
            float dist = distance2(location, maze[i].position);
            if (dist < distance) {
                closest = i;
                distance = dist;
            }
        }

        return closest;
    }
};

/**
 * END OF AI STUFF
 */

class GameHandler {
public:
    gDevice *device;
    gSceneManager *mngr;
    gResourceManager *rmngr;
    gSkybox *sky;
    gLight *light0;
    gLight *light1;
    gLight *light2;
    gLight *light3;
    gLight *light4;
    gLight *light5;
    gLight *light6;
    vector<gRigidBody *> cubes;

    gFPSCamera *cam1;
    gSpectatorCamera *cam2;
    gStaticCamera *cam3;

    Maze *maze;

    int width, height;
    int gameState;

    /**
     * Player sounds.
     */
    vector<gSound *> walkingSounds;
    gSound *jumpSound, *landSound;
    bool playerJumped;

    Weapon *rocketl;
    int kills;
    gSound *killSound;
    gSound *one_frag, *two_frags, *three_frags;
    gSound *death;
    gSound *victory;

    enum E_GAME_STATE {
        intro, fri, poweredBy, mainMenu, game, endGame
    };

public:
    GameHandler(int width, int height, bool fullscreen) {
        srand(time(NULL));

        kills = 0;

        this->width = width;
        this->height = height;
        device = gDevice::createDevice(width, height, fullscreen);

        if (!device) {
            cerr << "Error creating device!" << endl;
            exit(10);
        }

        mngr = device->getSceneManager();
        if (!mngr) {
            cerr << "Error initializing scene manager!" << endl;
            exit(12);
        }

        rmngr = mngr->getResourceManager();
        if (!rmngr) {
            cerr << "Error initializing resource manager!" << endl;
            exit(12);
        }

        /**
         * Add 3 cameras.
         */
        cam1 = (gFPSCamera *) mngr->addFPSCamera(vec3(0, -100, 0),
                                                 vec3(0.0f));
        string *data = new string("camera");
        cam1->setUserData((void *) data);
        cam1->setVelocity(80);

        cam2 = (gSpectatorCamera *) mngr->addSpectatorCamera(vec3(0, -20, 0),
                                                             vec3(0.0f));
        cam2->setVelocity(100);
        cam3 = (gStaticCamera *) mngr->addStaticCamera(vec3(0, 100, 0),
                                                       vec3(0, -3.14, 0));
        cam3->setRotation(vec3(180, -45, 0));

        rmngr->loadShader("default.vs", "default.frag", "default");
        rmngr->load3dModel("nanosuit/nanosuit.obj", "nanosuit",
                           aiProcess_CalcTangentSpace);
//			rmngr->load3dModel("TheWitcher/Geralt/Geralt.obj",
//					"geralt", aiProcess_CalcTangentSpace);
//			rmngr->load3dModel("knight/knight.obj", "knight",
//					aiProcess_CalcTangentSpace);
        rocketl = new Weapon(10, 2, "data/Sounds/rocket/rocklf1a.wav",
                             "Quake3/models/weapons2/rocketl/rocketl.md3", mngr, rmngr);

        gAnimator *anim = mngr->createRotationAnimator(vec3(1, 1, 1), radians(vec3(0, 100, 0)));

        maze = new Maze(cam1, mngr, rmngr, rocketl);

        /**
         * Init player sounds.
         */
        walkingSounds.push_back(mngr->createSound("data/Sounds/boot1.wav", false, EST_POSITIONAL));
        walkingSounds.push_back(mngr->createSound("data/Sounds/boot2.wav", false, EST_POSITIONAL));
        walkingSounds.push_back(mngr->createSound("data/Sounds/boot3.wav", false, EST_POSITIONAL));
        walkingSounds.push_back(mngr->createSound("data/Sounds/boot4.wav", false, EST_POSITIONAL));
        jumpSound = mngr->createSound("data/Sounds/jump1.wav", false, EST_AMBIENT);
        landSound = mngr->createSound("data/Sounds/land1.wav", false, EST_POSITIONAL);
        playerJumped = false;

        killSound = mngr->createSound("data/Sounds/hit.wav", false, EST_AMBIENT);
        one_frag = mngr->createSound("data/Sounds/1_frag.wav", false, EST_AMBIENT);
        two_frags = mngr->createSound("data/Sounds/2_frags.wav", false, EST_AMBIENT);
        three_frags = mngr->createSound("data/Sounds/3_frags.wav", false, EST_AMBIENT);
        death = mngr->createSound("data/Sounds/death.wav", false, EST_AMBIENT);

        /**
         * Init weapons.
         */
        // init python
        // todo load weapons using python scripts
//			Py_Initialize();
//			PyRun_SimpleString("import hello");
//			PyRun_SimpleString("hello.hello()");
//			Py_Finalize();

//			rocketl = new Weapon(10, "data/Sounds/rocket/rocklf1a.wav",
//					"Quake3/models/weapons2/railgun/railgun.md3", mngr, rmngr);

        gPathAnimator *cameraAnim = mngr->createPathAnimator(LINE, 0.2);
        cameraAnim->addPoint(vec3(0, 100, 0));
        cameraAnim->addPoint(vec3(0, 100, 100));
        cameraAnim->addPoint(vec3(0, 100, 0));
        cameraAnim->setLoop(true);
        cam3->addAnimator(cameraAnim);

        mngr->selectCamera(0);

        /**
         * Add objects to world.
         */

        gRigidBody *cube;
        rmngr->load3dModel("cube.obj", "cube", aiProcess_CalcTangentSpace);

        /**
         * Create level.
         */
        gRigidBody *level = mngr->addRigidBody(vec3(0, -50, 0), vec3(0.3, 0.3, 0.3), vec3(0, 0, 0));
        level->setShader(rmngr->getShader("default"));
        level->setRotation(vec3(-M_PI / 2, 0, M_PI));
        level->setMaterial(gMaterial::CHROME);
        rmngr->load3dModel("mapa/fy_snow.fbx", "level");
        level->setModel(rmngr->getModel("level"));

        /**
         * Create skybox.
         */
        sky = mngr->createSkybox("sky1/", ".tga");
        sky->setSize(50, 50, 50);
        sky->setPosition(0, 0, 0);
        sky->setRotation(vec3(-M_PI, 0, 0));

        /**
         * cubes displaying different materials
         */
        GLuint diff = rmngr->loadTexture("grass.tga", "normal3");
        GLuint norm = rmngr->loadTexture("rock.tga", "normal3_norm");

        for (int i = 0; i < 4; i++) {
            cube = mngr->addRigidBody(vec3(0, 50, -200 + i * 200), vec3(70, 70, 70), vec3(0, 0, 0));
            cube->setShader(rmngr->getShader("default"));
            cube->setDiffuseTexture(diff);
            cube->setSpecularTexture(diff);
            cube->setNormalMap(norm);
            cube->addAnimator(anim);
            cube->setModel(rmngr->getModel("cube"));
            cubes.push_back(cube);
        }

        for (int i = 0; i < 4; i++) {
            cube = mngr->addRigidBody(vec3(-200, 50, -200 + i * 200), vec3(70, 70, 70), vec3(0, 0, 0));
            cube->setShader(rmngr->getShader("default"));
            cube->setDiffuseTexture(diff);
            cube->setSpecularTexture(diff);
            cube->setNormalMap(norm);
            cube->addAnimator(anim);
            cube->setModel(rmngr->getModel("cube"));
            cubes.push_back(cube);
        }
        cubes[0]->setMaterial(gMaterial::BRASS);
        cubes[1]->setMaterial(gMaterial::BRONZE);
        cubes[2]->setMaterial(gMaterial::CHROME);
        cubes[3]->setMaterial(gMaterial::EMERALD);
        cubes[4]->setMaterial(gMaterial::OBSIDIAN);
        cubes[5]->setMaterial(gMaterial::RED_PLASTIC);
        cubes[6]->setMaterial(gMaterial::RUBY);
        cubes[7]->setMaterial(gMaterial::WHITE_RUBBER);

        gRigidBody *kocka = mngr->addRigidBody(vec3(100, 100, 100), vec3(3, 3, 3), vec3());
        kocka->setShader(rmngr->getShader("default"));
        kocka->setModel(rmngr->getModel("nanosuit"));

        /**
         * LIGHTS
         */
        light0 = mngr->addLightSource(vec3(-180, 200, 60),
                                      vec3(-0.2f, -1.0f, -0.3f), ELT_DIRECTIONAL);
        light0->setLight(vec3(1.0f, 1.0f, 1.0f), vec3(0.8f, 0.8f, 0.8f),
                         vec3(0.7f, 0.7f, 0.7f));
        light0->setModel(rmngr->load3dModel("cube.obj", "bulb"));
        light0->createShadowInfo();

        gPathAnimator *anim2 = mngr->createPathAnimator(LINE, 0.2f);
        anim2->addPoint(vec3(0.0, 50.0, -140.0));
        anim2->addPoint(vec3(-360.0, 50.0, -140.0));
        anim2->addPoint(vec3(-360.0, 50.0, 220.0));
        anim2->addPoint(vec3(0.0, 50.0, 220.0));
        anim2->addPoint(vec3(0.0, 50.0, -140.0));
        anim2->setLoop(true);
        light1 = mngr->addLightSource(vec3(-133.613007f, 0, -48.249157f), vec3(0, 0, 0), ELT_POINT);
        light1->setLight(vec3(1.0, 1.0, 0.2), vec3(0.5, 0.5, 0.5), vec3(0.7, 0.7, 0.2));
        light1->setAttenuation(1.0f, 0.14f, 0.07f);
        light1->addAnimator(anim2);

        light2 = mngr->addLightSource(cam1->getPosition(), cam1->getDirection(), ELT_SPOTLIGHT);
        light2->setLight(vec3(0.8, 0.8, 0.8), vec3(0.5, 0.5, 0.5), vec3(0.9, 0.9, 0.9));
        light2->setSpotlight(0.91, 0.82);
        light2->setVisible(false);

        vec3 ambient(0.05f, 0.05f, 0.05f);
        vec3 diffuse(0.2, 0.2, 0.9);
        vec3 specular(0.1, 0.1, 0.7f);
        light3 = mngr->addLightSource(vec3(70, -100, 300), vec3(0, 0, 0), ELT_POINT);
        light3->setLight(ambient, diffuse, specular);
        light3->setAttenuation(1.0f, 0.14f, 0.07f);

        light4 = mngr->addLightSource(vec3(-400, -100, 300), vec3(0, 0, 0), ELT_POINT);
        light4->setLight(ambient, diffuse, specular);
        light4->setAttenuation(1.0f, 0.14f, 0.07f);

        ambient = vec3(0.05f, 0.05f, 0.05f);
        diffuse = vec3(0.2, 0.9, 0.2);
        specular = vec3(0.1, 0.7, 0.1);
        light5 = mngr->addLightSource(vec3(-400, -100, -180), vec3(0, 0, 0), ELT_POINT);
        light5->setLight(ambient, diffuse, specular);
        light5->setAttenuation(1.0f, 0.14f, 0.07f);

        light6 = mngr->addLightSource(vec3(70, -100, -180), vec3(0, 0, 0), ELT_POINT);
        light6->setLight(ambient, diffuse, specular);
        light6->setAttenuation(1.0f, 0.14f, 0.07f);
    }

    void startGame() {
        mat4 ortho = glm::ortho((float) -width, (float) width,
                                (float) -height, (float) height, -100.0f, 100.0f);

        rmngr->loadShader("sprite.vs", "sprite.frag", "sprite");

        GLuint tex = rmngr->loadTexture("crosshair1.tga",
                                        "sprite");
        gSprite *crosshair = new gSprite(vec3(0, 0, 0), vec2(100, 100));
        crosshair->setColor(vec3(1, 1, 1));
        crosshair->setShader(rmngr->getShader("sprite"));
        crosshair->setTexture(tex);
        crosshair->setProjection(ortho);

        // testeram animator za gif
        gSprite *loading = new gSprite(vec3(0, 0, 0), vec2(300, 300));
        loading->setShader(rmngr->loadShader("sprite.vs", "sprite.frag", "sprite"));
        loading->setTexture(rmngr->loadTexture("loading.tga", "loadingAnim"));
        loading->setProjection(ortho);
        loading->enableTextureAnimation();

        gTextureAnimator *anim = new gTextureAnimator(8);
        anim->setFPS(15);
        anim->setLoop(true);
        loading->addAnimator(anim);

        gSprite poweredBySprite(vec3(0), vec2(600, 600));
        poweredBySprite.setTexture(
                rmngr->loadTexture("g3dLogo.tga", "logo"));
        poweredBySprite.setShader(rmngr->getShader("sprite"));
        poweredBySprite.setProjection(ortho);
        poweredBySprite.enableBlending();

        gSprite friSprite(vec3(0), vec2(620, 260));
        friSprite.setTexture(rmngr->loadTexture("friLogo.tga", "friLogo"));
        friSprite.setShader(rmngr->getShader("sprite"));
        friSprite.setProjection(ortho);
        friSprite.enableBlending();

        gFadeAnimator *fadeAnim = new gFadeAnimator(-0.01, 0.0);
        float startFadeDelay = 2.0f;
        poweredBySprite.addAnimator(fadeAnim);
        friSprite.addAnimator(fadeAnim);

        gSprite victoryText(vec3(0), vec2(1000, 600));
        victoryText.setTexture(
                rmngr->loadTexture("victory.png", "victory"));
        victoryText.setShader(rmngr->getShader("sprite"));
        victoryText.setProjection(ortho);
        victoryText.enableBlending();

        /**
         * Game state.
         */
        gameState = intro;
        float durations[] = {3.0, 4.0, 4.0, 0.1, -1.0, -1.0};
//			float durations[] = { 0.1, 0.1, 0.1, 0.1, -1.0, -1.0 };
        float stateStartTime = glfwGetTime();

        /**
         * Some game data.
         */
        gSound *welcome = mngr->createSound("data/Sounds/intro_01.wav",
                                            false, EST_AMBIENT);
        gSound *one = mngr->createSound("data/Sounds/one.wav", false,
                                        EST_AMBIENT);
        gSound *two = mngr->createSound("data/Sounds/two.wav", false,
                                        EST_AMBIENT);
        gSound *three = mngr->createSound("data/Sounds/three.wav", false,
                                          EST_AMBIENT);
        gSound *fight = mngr->createSound("data/Sounds/fight.wav", false,
                                          EST_AMBIENT);
        victory = mngr->createSound("data/Sounds/fight.wav", false,
                                    EST_AMBIENT);

        int currentSound = 0;
        float soundStartTime = 0.0f;
        float soundDurations[] = {4.0f, 1.7f, 1.7f, 1.7f, 1.7f};
//			float soundDurations[] = { 0.1f, 0.1f, 0.1f, 0.1f, 0.1f };

        float walkingDelay = 0.6f;
        float prevWalkingSound = 0.0f;

        bool gameStarted = false;

        while (device->isRunning()) {
            // check if ready to enter next state
            if (durations[gameState] > -1.0f
                && glfwGetTime() - stateStartTime
                   >= durations[gameState]) {
                gameState++;
                stateStartTime = soundStartTime = glfwGetTime();
                if (gameState == game) {
                    welcome->play();
                    device->begin();
                }
            }

            glViewport(0, 0, width, height);
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
            glClearColor(1, 1, 1, 1);

            if (gameState == intro) {
                loading->render();
                loading->update(1.0f / 60.0f);
            } else if (gameState == fri) {
                friSprite.render();
                if (glfwGetTime() - stateStartTime >= startFadeDelay)
                    friSprite.update(1.0f / 60.0f);
            } else if (gameState == poweredBy) {
                poweredBySprite.render();
                if (glfwGetTime() - stateStartTime >= startFadeDelay)
                    poweredBySprite.update(1.0f / 60.0f);
            } else if (gameState == game) {
                if (currentSound == 4)
                    device->begin();
                else {
                    if (currentSound < 4 && glfwGetTime() - soundStartTime >= soundDurations[currentSound]) {
                        currentSound++;
                        soundStartTime = glfwGetTime();
                        if (currentSound == 1)
                            three->play();
                        else if (currentSound == 2)
                            two->play();
                        else if (currentSound == 3)
                            one->play();
                        else if (currentSound == 4) {
                            fight->play();
                            gameStarted = true;
                        }
                    }
                }
                // draw scene
                mngr->drawScene();

                if (gameStarted) {
                    // update maze
                    maze->update();

                    // render weapon and crosshair
                    crosshair->render();
//						rocketl->weapon->setPosition(
//								mngr->getSelectedCamera()->getPosition());

                    // if player is moving, play moving sounds
                    vec3 vel = cam1->getVelocity();
                    vel.y = 0.0f;
                    if (cam1->isOnGround() && length2(vel) > 0.0f) {
                        if (glfwGetTime() - prevWalkingSound >= walkingDelay) {
                            prevWalkingSound = glfwGetTime();
                            int rnd = rand() % 4;
                            walkingSounds.at(rnd)->setPosition(cam1->getPosition());
                            walkingSounds.at(rnd)->play();
                        }
                    }
                    // if player landed on ground
                    if (playerJumped && cam1->isOnGround()) {
                        playerJumped = false;
                        landSound->setPosition(cam1->getPosition());
                        landSound->play();
                    }

                    light2->setPosition(cam1->getPosition());
                    light2->setRotation(cam1->getDirection());
                }
            } else if (gameState == endGame) {
                // draw scene
                mngr->drawScene();
                victoryText.render();
            }

            device->end();
        }
    }
};

GameHandler *game;

void collisionEventHandler(gRigidBody *a, gRigidBody *b) {
    string *dataA = NULL, *dataB = NULL;
    dataA = static_cast<string *>(a->getUserData());
    dataB = static_cast<string *>(b->getUserData());

    if (dataA && dataB) {
        if (*dataA == "player" && *dataB == "enemy") {
//            game->death->play();
        } else if (*dataA == "enemy" && *dataB == "bullet") {
            game->kills++;
            game->killSound->play();

            cout << "Kills: " << game->kills << endl;

            if (game->kills == 7)
                game->three_frags->play();
            else if (game->kills == 8)
                game->two_frags->play();
            else if (game->kills == 9)
                game->one_frag->play();
            else if (game->kills == 10) {
                game->gameState = GameHandler::endGame;
                game->victory->play();
            }
            b->setDrawable(false);
        }
    }
}

void handleMouseClick(int key, int action, int mods) {
    if (action == GLFW_PRESS) {
        if (key == GLFW_MOUSE_BUTTON_LEFT) {
            vec3 dir = game->cam1->getDirection();
            vec3 pos = game->cam1->getPosition();
            game->rocketl->onFire(pos, dir);
        }
    }
}

void handleKeyPress(int key, int code, int action, int mods) {
    if (action == GLFW_PRESS) {
        switch (key) {
            case GLFW_KEY_ENTER:
                if (game->gameState < 4)
                    game->gameState++;
                break;
            case GLFW_KEY_SPACE:
                if (game->cam1->isOnGround()) {
                    game->jumpSound->setPosition(game->cam1->getPosition());
                    game->jumpSound->play();
                    game->playerJumped = true;
                }
                break;

            case GLFW_KEY_1:
                game->light0->switchLight();
                break;
            case GLFW_KEY_2:
                game->light1->switchLight();
                break;
            case GLFW_KEY_3:
                game->light2->switchLight();
                break;
            case GLFW_KEY_4:
                game->light3->switchLight();
                break;
            case GLFW_KEY_5:
                game->light4->switchLight();
                break;
            case GLFW_KEY_6:
                game->light5->switchLight();
                break;
            case GLFW_KEY_7:
                game->light6->switchLight();
                break;
            case GLFW_KEY_P:
                cout << "Pos: "
                     << to_string(game->mngr->getSelectedCamera()->getPosition())
                     << endl
                     << to_string(game->mngr->getSelectedCamera()->getRotation())
                     << endl << endl;
                break;
            case GLFW_KEY_K:
                glEnable(GL_FRAMEBUFFER_SRGB);
                break;
            case GLFW_KEY_L:
                glDisable(GL_FRAMEBUFFER_SRGB);
                break;
            case GLFW_KEY_Z:
                game->mngr->switchPostProcessingEffect(gSceneManager::EET_INVERSION);
                break;
            case GLFW_KEY_X:
                game->mngr->switchPostProcessingEffect(gSceneManager::EET_GRAYSCALE);
                break;
            case GLFW_KEY_C:
                game->mngr->switchPostProcessingEffect(gSceneManager::EET_BLUR);
                break;
            case GLFW_KEY_V:
                game->mngr->switchPostProcessingEffect(gSceneManager::EET_EDGEDETECTION);
                break;
            case GLFW_KEY_B:
                game->mngr->switchPostProcessingEffect(gSceneManager::EET_KERNEL);
                break;
            case GLFW_KEY_N:
                game->mngr->switchPostProcessingEffect(gSceneManager::EET_SSAO);
                break;
            case GLFW_KEY_M:
                game->mngr->switchPostProcessingEffect(gSceneManager::EET_BLOOM);
                break;
            case GLFW_KEY_E:
                game->mngr->selectCamera(0);
                break;
            case GLFW_KEY_R:
                game->mngr->selectCamera(1);
                break;
            case GLFW_KEY_T:
                game->mngr->selectCamera(2);
                break;
            case 343:
            case 347:
                game->cam1->setClip(true);
                break;
            case GLFW_KEY_LEFT_ALT:
                for (int i = 0; i < game->cubes.size(); i++)
                    game->cubes[i]->switchVisibility();
                break;
        }
    } else {
        switch (key) {
            case 343:
                game->cam1->setClip(false);
                break;
        }
    }
}

int main(int argc, char **argv) {
    int width = 1920 * 2, height = 1080 * 2;
    game = new GameHandler(width, height, false);
    game->mngr->setKeyPressHandler(handleKeyPress);
    game->mngr->setMousePressHandler(handleMouseClick);
    game->mngr->setCollisionEventHandler(collisionEventHandler);
    game->startGame();
}
