#include "gDevice.h"

using namespace std;
#define BUFFER_OFFSET(i) ((char*)NULL + (i))

gDevice *gDevice::m_pSingleton = NULL;

gDevice::gDevice(int width, int height, bool fullscreen) {
    m_iWidth = width;
    m_iHeight = height;
    m_sWindowName = "G3dEngine -";
    m_pWindow = NULL;
    m_iFrameCount = 0;
    m_iNumberOfFrames = 60;
    m_bMotionBlur = false;

    if (!glfwInit()) {
        cerr << "Glfw could not be initialized!" << endl;
        return;
    }

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);  // yes, 3 and 2!!!
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_SAMPLES, 4);

    // monitors
    int count;
    GLFWmonitor **monitors = glfwGetMonitors(&count);
    GLFWmonitor *monitor;
//	if (count > 1)
//		monitor = monitors[1];
//	else
    monitor = monitors[0];
    if (!fullscreen)
        monitor = NULL;

    m_pWindow = glfwCreateWindow(m_iWidth / 2, m_iHeight / 2, m_sWindowName.c_str(), monitor, NULL);
    if (!m_pWindow) {
        glfwTerminate();
        cerr << "Glfw window could not be initialized!" << endl;
        return;
    }
    // disable cursor
    glfwSetInputMode(m_pWindow, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
//	glfwSetCursor(m_pWindow, glfwCreateStandardCursor(GLFW_CROSSHAIR_CURSOR));

    m_fPreviousTimeS = glfwGetTime();

    glfwMakeContextCurrent(m_pWindow);
    glfwSwapInterval(1);

    ilInit();
    iluInit();

    cout << "OpenGL Version: " << glGetString(GL_VERSION) << endl;
    cout << "GLSL Version: " << glGetString(GL_SHADING_LANGUAGE_VERSION)
         << endl;

    // Register GLFW callbacks
    glfwSetMouseButtonCallback(m_pWindow, onMousePressEvent);
    glfwSetCursorPosCallback(m_pWindow, onMouseMoveEvent);
    glfwSetKeyCallback(m_pWindow, onKeyPressEvent);

    glfwGetCursorPos(m_pWindow, &m_fMouse_x, &m_fMouse_y);

    m_pWorld = new gSceneManager(m_iWidth, m_iHeight, m_pWindow);
}

gDevice *gDevice::createDevice(int width, int height, bool fullscreen) {
    gDevice *dev = new gDevice(width, height, fullscreen);
    gDevice::setSingleton(dev);
    return dev;
}

void gDevice::begin() {
    gFPSCounter::calcFPS(m_pWindow, 2.0, m_sWindowName);

    double currentTime = glfwGetTime();
    m_pWorld->updateScene(currentTime - m_fPreviousTimeS);
    m_fPreviousTimeS = currentTime;
}

void gDevice::end() {
    if (m_bMotionBlur) {
        if (m_iFrameCount)
            glAccum(GL_LOAD, 1.0 / m_iNumberOfFrames);
        else
            glAccum(GL_ACCUM, 1.0 / m_iNumberOfFrames);

        m_iFrameCount++;

        if (m_iFrameCount >= m_iNumberOfFrames) {
            m_iFrameCount = 0;
            glAccum(GL_RETURN, 1.0);
            glfwSwapBuffers(m_pWindow);
        }
    } else
        glfwSwapBuffers(m_pWindow);

    glfwPollEvents();
}

void gDevice::clearData() {
    glfwDestroyWindow(m_pWindow);
    glfwTerminate();
    delete m_pWorld;
}

void gDevice::onKeyPressEvent_(GLFWwindow *window, int key, int code,
                               int action, int mods) {
    m_pWorld->handleKeyPres(key, code, action, mods);
}

void gDevice::onMousePressEvent_(GLFWwindow *window, int key, int action,
                                 int mods) {
    m_pWorld->handleMousePress(key, action, mods);
}

void gDevice::onMouseMoveEvent_(GLFWwindow *window, double x, double y) {
    m_pWorld->handleMouseMove(x, y);
}

void gDevice::onKeyPressEvent(GLFWwindow *window, int key, int code, int action,
                              int mods) {
    gDevice::m_pSingleton->onKeyPressEvent_(window, key, code, action, mods);
}

void gDevice::onMousePressEvent(GLFWwindow *window, int key, int action,
                                int mods) {
    gDevice::m_pSingleton->onMousePressEvent_(window, key, action, mods);
}

void gDevice::onMouseMoveEvent(GLFWwindow *window, double x, double y) {
    gDevice::m_pSingleton->onMouseMoveEvent_(window, x, y);
}
