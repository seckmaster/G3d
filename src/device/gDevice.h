#ifndef SRC_DEVICE_
#define SRC_DEVICE_

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <OpenGL/gl3.h>
#include <stdlib.h>
#include <string>
#include <iostream>
#include <IL/il.h>
#include <IL/ilut.h>

#include "gFPSCounter.h"
#include "../scene/gSceneManager.h"

class gDevice {
public:
    gDevice(int width, int height, bool fullscreen = false);

    static gDevice *createDevice(int width, int height, bool fullscreen =
    false);

    inline void setWindowTitle(std::string title) {
        m_sWindowName = title;
    }

    inline gSceneManager *getSceneManager() {
        return m_pWorld;
    }

    inline bool isRunning() {
        return !glfwWindowShouldClose(m_pWindow);
    }

    void begin();

    void end();

    void clearData();

private:
    static void setSingleton(gDevice *ptr) {
        m_pSingleton = ptr;
    }

private:

    void onRender();

    void onUpdate(double deltaTimeMS);

    static void onKeyPressEvent(GLFWwindow *window, int key, int code, int action, int mods);

    static void onMousePressEvent(GLFWwindow *window, int key, int action,
                                  int mods);

    static void onMouseMoveEvent(GLFWwindow *window, double x, double y);

public:
    void onKeyPressEvent_(GLFWwindow *window, int key, int code, int action,
                          int mods);

    void onMousePressEvent_(GLFWwindow *window, int key, int action,
                            int mods);

    void onMouseMoveEvent_(GLFWwindow *window, double x, double y);

private:
    GLFWwindow *m_pWindow;
    double m_fMouse_x, m_fMouse_y;
    double m_fPreviousTimeS;
    float m_fSensitivity = 0.1;

    int m_iWidth;
    int m_iHeight;
    std::string m_sWindowName;

    gSceneManager *m_pWorld;

    GLuint VBO;
    GLuint VAO;

private:
    // motion blur stuff
    int m_iNumberOfFrames;
    int m_iFrameCount;
    bool m_bMotionBlur;

private:
    static gDevice *m_pSingleton;
};

#endif /* SRC_DEVICE_ */
