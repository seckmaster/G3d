#ifndef SRC_SCENE_GTRANSFORMATION_H_
#define SRC_SCENE_GTRANSFORMATION_H_

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/std_based_type.hpp>
#include <glm/gtx/vector_angle.hpp>
#include <glm/gtx/compatibility.hpp>
#include <glm/gtx/matrix_operation.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/euler_angles.hpp>
#include <glm/ext.hpp>

class gTransformation {
public:
    gTransformation();

    gTransformation(const glm::vec3 &position, const glm::vec3 &rotation,
                    const glm::vec3 &scale);

    gTransformation(const gTransformation &o);

    inline void rotate(const glm::vec3 &rot) { m_vRotation += rot; }

    inline void translate(const glm::vec3 &tr) { m_vPosition += tr; }

    inline void scale(const glm::vec3 &sc) { m_vScale += sc; }

    inline void setRotation(const glm::vec3 &rot) { m_vRotation = rot; }

    inline void setPosition(const glm::vec3 &tr) { m_vPosition = tr; }

    inline void setSize(const glm::vec3 &sc) { m_vScale = sc; }

    inline const glm::vec3 &getRotation() const { return m_vRotation; }

    inline const glm::vec3 &getPosition() const { return m_vPosition; }

    inline const glm::vec3 &getSize() const { return m_vScale; }

    glm::mat4 generateModelMatrix() const;

private:
    glm::vec3 m_vPosition;
    glm::vec3 m_vRotation;
    glm::vec3 m_vScale;
};

#endif /* SRC_SCENE_GTRANSFORMATION_H_ */
