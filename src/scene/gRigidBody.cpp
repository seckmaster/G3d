#include "gRigidBody.h"

#include "animator/gAnimator.h"
#include "gLight.h"

gRigidBody::gRigidBody(gAABB *shape) :
        gSceneNode(), m_pAABB(shape) {
    setSize(1, 1, 1);
    m_bNormalMapping = false;
    m_bParallaxMapping = false;
    m_oShader = nullptr;
}

gRigidBody::~gRigidBody() {
}

void gRigidBody::render() const {
    // dont render if invisible
    if (!m_bVisible || !m_bDrawable)
        return;

    glm::mat4 model = m_oTranformation.generateModelMatrix();

    if (m_oShader != nullptr) {
        m_oShader->useProgram();
        m_oShader->setMatrix4f("model", model);
        m_oShader->setMatrix4f("view", m_mView);
        m_oShader->setMatrix4f("projection", m_mProjection);
        m_oShader->setVector3f("viewPos", m_vCameraPos);

        m_oShader->setInteger1i("normalMapping", m_bNormalMapping);
        m_oShader->setInteger1i("parallaxMapping", m_bParallaxMapping);

        m_oShader->setVector3f("material.ambient", m_oMaterial.ambient);
        m_oShader->setVector3f("material.diffuse", m_oMaterial.diffuse);
        m_oShader->setVector3f("material.specular", m_oMaterial.specular);
        m_oShader->setFloat1f("material.shininess", m_oMaterial.shininess);
    }

    std::vector<gLight *>::const_iterator it = m_vLightSources.begin();
    int i = 0;
    for (; it != m_vLightSources.end(); ++i, ++it) {
        if (!(*it)->isEnabled()) {
            // light is disabled
            if (m_oShader != nullptr) {
                m_oShader->setInteger1i("lights[" + std::to_string(i) + "].isEnabled", false);
            }
            continue;
        }

        Light light = (*it)->getLight();
        if (m_oShader != nullptr) {
            if ((*it)->getShadowInfo()) {
                m_oShader->setMatrix4f("lightSpaceMatrix", (*it)->getShadowInfo()->getProjection());
            }
            m_oShader->setInteger1i("lights[" + std::to_string(i) + "].isEnabled", true);
            m_oShader->setVector3f("lights[" + std::to_string(i) + "].ambient", light.ambient);
            m_oShader->setVector3f("lights[" + std::to_string(i) + "].diffuse", light.diffuse);
            m_oShader->setVector3f("lights[" + std::to_string(i) + "].specular", light.specular);
            m_oShader->setInteger1i("lights[" + std::to_string(i) + "].type", (*it)->getType());
            m_oShader->setVector3f("lights[" + std::to_string(i) + "].position", (*it)->getPosition());
            m_oShader->setVector3f("lights[" + std::to_string(i) + "].direction", (*it)->getRotation());
            m_oShader->setFloat1f("lights[" + std::to_string(i) + "].constant", light.constant);
            m_oShader->setFloat1f("lights[" + std::to_string(i) + "].linear", light.linear);
            m_oShader->setFloat1f("lights[" + std::to_string(i) + "].quadratic", light.qudratic);
            m_oShader->setFloat1f("lights[" + std::to_string(i) + "].cutOff", light.cutOff);
            m_oShader->setFloat1f("lights[" + std::to_string(i) + "].outerCutOff", light.outerCutOff);
        }

        if ((*it)->getShadowInfo()) {
            glActiveTexture(GL_TEXTURE4);
            glBindTexture(GL_TEXTURE_2D, (*it)->getShadowInfo()->getShadowMap());
        }
    }

    if (m_iDiffuseTexture) {
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, m_iDiffuseTexture);
    }
    if (m_iSpecularTexture) {
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, m_iSpecularTexture);
    }
    if (m_iNormalTexture) {
        glActiveTexture(GL_TEXTURE2);
        glBindTexture(GL_TEXTURE_2D, m_iNormalTexture);
    }
    if (m_iDisplacmentTexture) {
        glActiveTexture(GL_TEXTURE3);
        glBindTexture(GL_TEXTURE_2D, m_iDisplacmentTexture);
    }

    if (m_pModel)
        m_pModel->Draw(*m_oShader);

    std::vector<gSceneNode *>::const_iterator it2 = m_vChildren.begin();
    for (; it2 != m_vChildren.end(); ++it2) {
        (*it2)->render();
    }

    glBindTexture(GL_TEXTURE_2D, 0);
}

void gRigidBody::render(gShader *shader) const {
//	glDisable(GL_DEPTH_TEST);
    // dont render if invisible
    if (!m_bVisible || !m_bDrawable)
        return;

    glm::mat4 model;
    model = glm::translate(model, getPosition());
    model = glm::rotate(model, getRotation().x, glm::vec3(1.0f, 0.0f, 0.0f));
    model = glm::rotate(model, getRotation().y, glm::vec3(0.0f, 1.0f, 0.0f));
    model = glm::rotate(model, getRotation().z, glm::vec3(0.0f, 0.0f, 1.0f));
    model = glm::scale(model, getSize());

    shader->useProgram();
    shader->setMatrix4f("model", model);
//	shader->setInteger1i("normalMapping", m_bNormalMapping);
    shader->setMatrix4f("view", m_mView);
    shader->setMatrix4f("projection", m_mProjection);
//	shader->setVector3f("viewPos", m_vCameraPos);

//	shader->setInteger1i("normalMapping", m_bNormalMapping);
//
//	shader->setVector3f("material.ambient", m_oMaterial.ambient);
//	shader->setVector3f("material.diffuse", m_oMaterial.diffuse);
//	shader->setVector3f("material.specular", m_oMaterial.specular);
//	shader->setFloat1f("material.shininess", m_oMaterial.shininess);
//
//	if (m_iDiffuseTexture) {
//		glActiveTexture(GL_TEXTURE0);
//		glBindTexture(GL_TEXTURE_2D, m_iDiffuseTexture);
//	}
//	if (m_iSpecularTexture) {
//		glActiveTexture(GL_TEXTURE1);
//		glBindTexture(GL_TEXTURE_2D, m_iSpecularTexture);
//	}
//	if (m_iNormalTexture) {
//		glActiveTexture(GL_TEXTURE2);
//		glBindTexture(GL_TEXTURE_2D, m_iNormalTexture);
//	}
//	if (m_iDisplacmentTexture) {
//		glActiveTexture(GL_TEXTURE3);
//		glBindTexture(GL_TEXTURE_2D, m_iDisplacmentTexture);
//	}

    if (m_pModel)
        m_pModel->Draw(*shader);

    glBindTexture(GL_TEXTURE_2D, 0);
//	glEnable(GL_DEPTH_TEST);
}

void gRigidBody::update(float deltaTimeMS) {
    if (!m_bDrawable)
        return;

    std::vector<gAnimator *>::const_iterator it = m_vAnimators.begin();
    for (; it != m_vAnimators.end(); ++it) {
        (*it)->animateNode(this, deltaTimeMS);
    }

    std::vector<gSceneNode *>::const_iterator it2 = m_vChildren.begin();
    for (; it2 != m_vChildren.end(); ++it2) {
        (*it2)->update(deltaTimeMS);
    }

    if (m_pAABB)
        m_pAABB->setPosition(getPosition());
}
