#include "gMesh.h"

using namespace std;

gMesh::gMesh(vector<Vertex> vertices, vector<GLuint> indices,
             vector<Texture> textures) {
    this->vertices = vertices;
    this->indices = indices;
    this->textures = textures;

    setupMesh();
}

void gMesh::setupMesh() {
    glGenVertexArrays(1, &this->VAO);
    glGenBuffers(1, &this->VBO);
    glGenBuffers(1, &this->EBO);

    glBindVertexArray(this->VAO);
    glBindBuffer(GL_ARRAY_BUFFER, this->VBO);

    glBufferData(GL_ARRAY_BUFFER, this->vertices.size() * sizeof(Vertex),
                 &this->vertices[0], GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, this->indices.size() * sizeof(GLuint),
                 &this->indices[0], GL_STATIC_DRAW);

    // Vertex Positions
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex),
                          (GLvoid *) 0);
    // Vertex Normals
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex),
                          (GLvoid *) offsetof(Vertex, Normal));
    // Vertex Texture Coords
    glEnableVertexAttribArray(2);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex),
                          (GLvoid *) offsetof(Vertex, TexCoords));
    // Vertex Tangents
    glEnableVertexAttribArray(3);
    glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex),
                          (GLvoid *) offsetof(Vertex, Tangents));
    // Vertex Bitangents
    glEnableVertexAttribArray(4);
    glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex),
                          (GLvoid *) offsetof(Vertex, biTangents));

    glBindVertexArray(0);
}

void gMesh::Draw(gShader shader) {
    GLuint diffuseNr = 1;
    GLuint specularNr = 1;
    GLuint normalNr = 1;
    for (GLuint i = 0; i < this->textures.size(); i++) {
        string number;
        string name = this->textures.at(i).type;
        if (name == "texture_diffuse") {
            number = to_string(diffuseNr++); // Transfer GLuint to stream
            glActiveTexture(GL_TEXTURE0); // Activate proper texture unit before binding

        } else if (name == "texture_specular") {
            number = to_string(specularNr++); // Transfer GLuint to stream
            glActiveTexture(GL_TEXTURE1); // Activate proper texture unit before binding
        } else if (name == "texture_normal") {
            number = to_string(normalNr++); // Transfer GLuint to stream
            glActiveTexture(GL_TEXTURE2); // Activate proper texture unit before binding
        }

        shader.setFloat1f(name + number, i);

        glBindTexture(GL_TEXTURE_2D, this->textures.at(i).id);
    }

    // Draw mesh
    glBindVertexArray(this->VAO);
    glDrawElements(GL_TRIANGLES, this->indices.size(), GL_UNSIGNED_INT, 0);
    glBindVertexArray(0);
    glBindTexture(GL_TEXTURE_2D, 0);
}
