#ifndef SRC_MODEL_GMESH_H_
#define SRC_MODEL_GMESH_H_

#include "../shader/gShader.h"
#include <vector>
#include <glm/glm.hpp>
#include <OpenGl/gl3.h>

typedef struct {
    glm::vec3 Position;
    glm::vec3 Normal;
    glm::vec2 TexCoords;
    glm::vec3 Tangents;
    glm::vec3 biTangents;
} Vertex;

typedef struct {
    GLuint id;
    std::string type;
    std::string path;
} Texture;

class gMesh {
public:
    /*  Mesh Data  */
    std::vector<Vertex> vertices;
    std::vector<GLuint> indices;
    std::vector<Texture> textures;

    /*  Functions  */
    gMesh(std::vector<Vertex> vertices, std::vector<GLuint> indices,
          std::vector<Texture> textures);

    void Draw(gShader shader);

private:
    /*  Render data  */
    GLuint VAO, VBO, EBO;

    /*  Functions    */
    void setupMesh();
};

#endif /* SRC_MODEL_GMESH_H_ */
