
#ifndef SRC_MODEL_GMODEL3D_H_
#define SRC_MODEL_GMODEL3D_H_

#include <iostream>
#include <vector>
#include "gMesh.h"

class gModel3D {
public:
    gModel3D() {}

    inline void Draw(gShader shader) {
        for (GLuint i = 0; i < this->meshes.size(); i++)
            this->meshes.at(i).Draw(shader);
    }

    /*  Model Data  */
    std::vector<gMesh> meshes;
    std::string directory;
};

#endif /* SRC_MODEL_GMODEL3D_H_ */
