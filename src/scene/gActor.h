#ifndef SRC_WORLD_INODE_
#define SRC_WORLD_INODE_

#include "glm/glm.hpp"
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/std_based_type.hpp>
#include <glm/gtx/vector_angle.hpp>
#include <glm/gtx/compatibility.hpp>
#include <glm/gtx/matrix_operation.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/euler_angles.hpp>
#include <glm/ext.hpp>

#include <OpenGL/gl3.h>
#include <OpenGL/glu.h>

#include <vector>
#include <string>
#include <iostream>
#include <fstream>

#include "shader/gShader.h"
#include "gMaterial.h"
#include "gTransformation.h"

class gActor {
public:
    gActor() : m_pUserData(NULL) {
    }

    virtual ~gActor();

    inline void setPosition(const glm::vec3 &pos) {
        m_oTranformation.setPosition(pos);
    }

    inline void setPosition(float x, float y, float z) {
        m_oTranformation.setPosition(glm::vec3(x, y, z));
    }

    inline void setRotation(const glm::vec3 &rotation) {
        m_oTranformation.setRotation(rotation);
    }

    inline void setRotation(float x, float y, float z) {
        m_oTranformation.setRotation(glm::vec3(x, y, z));
    }

    inline const glm::vec3 &getPosition() const {
        return m_oTranformation.getPosition();
    }

    inline const glm::vec3 &getRotation() const {
        return m_oTranformation.getRotation();
    }

    inline const glm::vec3 &getSize() const {
        return m_oTranformation.getSize();
    }

    inline void setSize(const glm::vec3 &size) {
        m_oTranformation.setSize(size);
    }

    inline void setSize(float x, float y, float z) {
        m_oTranformation.setSize(glm::vec3(x, y, z));
    }

    inline void setUserData(void *data) {
        m_pUserData = data;
    }

    inline void *getUserData() {
        if (m_pUserData)
            return m_pUserData;
        else return NULL;
    }

protected:
    gTransformation m_oTranformation;

    void *m_pUserData;
};

#endif /* SRC_WORLD_INODE_ */
