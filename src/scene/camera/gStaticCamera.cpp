#include "gStaticCamera.h"

gStaticCamera::gStaticCamera(const glm::vec3 &pos, const glm::vec3 &rotation) {
    setPosition(pos);
    setRotation(rotation);
    updateCameraVectors();
}

void gStaticCamera::update(float deltaTimeMS) {
    std::vector<gAnimator *>::const_iterator it = m_vAnimators.begin();
    for (; it != m_vAnimators.end(); ++it) {
        (*it)->animateNode(this, deltaTimeMS);
    }

    gActor::setRotation(cos(m_vLookAtPoint.x - getPosition().x),
                        sin(m_vLookAtPoint.y - getPosition().y),
                        0);
    updateCameraVectors();
}

void gStaticCamera::ProcessMouseMovement(GLfloat xoffset, GLfloat yoffset,
                                         GLboolean constrainPitch) {

}
