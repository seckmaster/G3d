#include "gSpectatorCamera.h"

#include <iostream>

using namespace std;

gSpectatorCamera::gSpectatorCamera(glm::vec3 position) :
        gCamera(position) {
}

void gSpectatorCamera::update(float deltaTimeMS) {
    GLfloat velocity = m_fMovementSpeed * deltaTimeMS;
    glm::vec3 vel = m_vRight * (velocity * m_vDirection.x) +
                    m_vFront * (velocity * m_vDirection.z);

    // update position
    m_oTranformation.translate(vel);
}

void gSpectatorCamera::addData(void *data) {

}
