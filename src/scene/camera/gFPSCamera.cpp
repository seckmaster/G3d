#include "gFPSCamera.h"

#include <iostream>

using namespace std;

gFPSCamera::gFPSCamera(const glm::vec3 &position) :
        gCamera(position) {
    m_fYoffset = 25;
    m_fGround = position.y;
    m_oTranformation.translate(glm::vec3(0, m_fYoffset, 0));

    m_bOnGround = true;
    m_vForce = glm::vec3(0.0f);
    m_bNoClip = false;

    m_pAABB = new gAABB(position, vec3(10, 50, 10));
}

void gFPSCamera::update(float deltaTimeMS) {
    m_vVelocity = m_vRight * (m_fMovementSpeed * m_vDirection.x)
                  + m_vFront * (m_fMovementSpeed * m_vDirection.z);

    if (!m_bNoClip) {
        m_vVelocity.y = 0.0f;

        // initiate jump
        if (m_vDirection.y == 1.0f && m_bOnGround) {
            m_vForce.y = 80.0f;
            m_vDirection.y = 0;
            m_bOnGround = false;
        }

        m_vForce.y -= 180.0f * deltaTimeMS;
    } else
        m_vForce.y = 0.0f;

    // update position
    m_oTranformation.translate((m_vVelocity + m_vForce) * deltaTimeMS);

    // prevent falling
    if (!m_bNoClip && getPosition().y <= m_fGround) {
        m_bOnGround = true;
        setPosition(getPosition().x, m_fGround, getPosition().z);
    }

    m_pAABB->setPosition(getPosition());
}

void gFPSCamera::addData(void *data) {
}
