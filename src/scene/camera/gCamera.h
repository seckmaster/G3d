#ifndef SRC_CAMERA_CAMERA_H_
#define SRC_CAMERA_CAMERA_H_

#include <iostream>
#include "../gActor.h"
#include "../animator/gAnimator.h"

// Defines several possible options for camera movement. Used as abstraction to stay away from window-system specific input methods
enum E_CAMERA_MOVEMENT {
    FORWARD, BACKWARD, LEFT, RIGHT, JUMP
};

// Default camera values
const GLfloat YAW = 0.0f;
const GLfloat PITCH = 0.0f;
const GLfloat SPEED = 90.0f;
const GLfloat SENSITIVTY = 0.1f;
const GLfloat ZOOM = 45.0f;

class gCamera : public gActor {
public:
    gCamera(const glm::vec3 &position = glm::vec3(0.0f),
            const glm::vec3 &up = glm::vec3(0.0f, 1.0f, 0.0f),
            const glm::vec3 &rotation = glm::vec3(0.0f));

    gCamera(GLfloat posX, GLfloat posY, GLfloat posZ, GLfloat upX,
            GLfloat upY, GLfloat upZ, GLfloat yaw, GLfloat pitch,
            GLfloat roll);

    virtual void update(float deltaTimeS) = 0;

    virtual void addData(void *data) = 0;

    virtual ~gCamera() {
    }

    const glm::mat4 &getViewMatrix();

    void ProcessKeyboard(E_CAMERA_MOVEMENT direction, bool pressed = true);

    virtual void ProcessMouseMovement(GLfloat xoffset, GLfloat yoffset,
                                      GLboolean constrainPitch = true);

    inline void setVelocity(float v) {
        m_fMovementSpeed = v;
    }

    inline void addAnimator(gAnimator *anim) {
        m_vAnimators.push_back(anim);
    }

    inline void setRotation(const glm::vec3 &rot) {
        m_oTranformation.setRotation(rot);
        updateCameraVectors();
    }

    inline const glm::vec3 &getDirection() {
        return m_vFront;
    }

    inline const glm::vec3 &getVelocity() {
        return m_vVelocity;
    }

protected:
    void updateCameraVectors();

protected:
    glm::vec3 m_vDirection; // movement
    // camera velocity and force
    glm::vec3 m_vForce;
    glm::vec3 m_vVelocity;

    // vectors
    glm::vec3 m_vFront;
    glm::vec3 m_vUp;
    glm::vec3 m_vRight;
    glm::vec3 m_vWorldUp;

    // Camera options
    GLfloat m_fMovementSpeed;
    GLfloat m_fMouseSensitivity;
    GLfloat m_fZoom;

    std::vector<gAnimator *> m_vAnimators;
};

#endif /* SRC_CAMERA_CAMERA_H_ */
