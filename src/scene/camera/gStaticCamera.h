#ifndef SRC_SCENE_CAMERA_GSTATICCAMERA_H_
#define SRC_SCENE_CAMERA_GSTATICCAMERA_H_

#include "gCamera.h"

class gStaticCamera : public gCamera {
public:
    gStaticCamera(const glm::vec3 &pos, const glm::vec3 &rotation);

    void update(float deltaTimeMS);

    void addData(void *d) {
    }

    void ProcessMouseMovement(GLfloat xoffset, GLfloat yoffset,
                              GLboolean constrainPitch = true);

    inline void setLookAt(const glm::vec3 &point) {
        m_vLookAtPoint = point;
    }

private:
    glm::vec3 m_vLookAtPoint;
};

#endif /* SRC_SCENE_CAMERA_GSTATICCAMERA_H_ */
