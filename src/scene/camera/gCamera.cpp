#include "gCamera.h"

gCamera::gCamera(const glm::vec3 &position, const glm::vec3 &up,
                 const glm::vec3 &rotation) :
        gActor(), m_vFront(glm::vec3(0.0f, 0.0f, -1.0f)), m_fMovementSpeed(
        SPEED), m_fMouseSensitivity(SENSITIVTY), m_fZoom(ZOOM) {
    this->m_vWorldUp = up;
    this->m_oTranformation.setPosition(position);
    this->m_oTranformation.setRotation(rotation);
    this->updateCameraVectors();
}

gCamera::gCamera(GLfloat posX, GLfloat posY, GLfloat posZ, GLfloat upX,
                 GLfloat upY, GLfloat upZ, GLfloat yaw, GLfloat pitch, GLfloat roll) :
        gActor(), m_vFront(glm::vec3(0.0f, 0.0f, -1.0f)), m_fMovementSpeed(SPEED), m_fMouseSensitivity(
        SENSITIVTY), m_fZoom(ZOOM) {
    this->m_vWorldUp = glm::vec3(upX, upY, upZ);
    this->m_oTranformation.setPosition(glm::vec3(posX, posY, posZ));
    this->m_oTranformation.setRotation(glm::vec3(yaw, pitch, roll));
    this->updateCameraVectors();
}

// Returns the view matrix calculated using Eular Angles and the LookAt Matrix
const glm::mat4 &gCamera::getViewMatrix() {
    return glm::lookAt(this->getPosition(), this->getPosition() + this->m_vFront,
                       this->m_vUp);
}

void gCamera::ProcessKeyboard(E_CAMERA_MOVEMENT direction, bool pressed) {
    if (direction == FORWARD)
        m_vDirection.z = pressed ? 1 : 0;
    else if (direction == BACKWARD)
        m_vDirection.z = pressed ? -1 : 0;
    else if (direction == LEFT)
        m_vDirection.x = pressed ? -1 : 0;
    else if (direction == RIGHT)
        m_vDirection.x = pressed ? 1 : 0;
    else if (direction == JUMP)
        m_vDirection.y = 1;
}

void gCamera::ProcessMouseMovement(GLfloat xoffset, GLfloat yoffset,
                                   GLboolean constrainPitch) {
    xoffset *= this->m_fMouseSensitivity;
    yoffset *= this->m_fMouseSensitivity;

//	this->m_vRotation.x += xoffset;
//	this->m_vRotation.y += yoffset;
    m_oTranformation.rotate(glm::vec3(xoffset, yoffset, 0));

    // Make sure that when pitch is out of bounds, screen doesn't get flipped
    if (constrainPitch) {
        if (getRotation().y > 89.0f)
            gActor::setRotation(getRotation().x, 89.0, getRotation().z);
        if (m_oTranformation.getRotation().y < -89.0f)
            gActor::setRotation(getRotation().x, -89.0, getRotation().z);
    }

    // Update Front, Right and Up Vectors using the updated Eular angles
    this->updateCameraVectors();
}

void gCamera::updateCameraVectors() {
    // Calculate the new Front vector
    glm::vec3 front;
    glm::vec3 rotation = m_oTranformation.getRotation();
    front.x = cos(glm::radians(rotation.x))
              * cos(glm::radians(rotation.y));
    front.y = sin(glm::radians(rotation.y));
    front.z = sin(glm::radians(rotation.x))
              * cos(glm::radians(rotation.y));
    this->m_vFront = glm::normalize(front);
    // Also re-calculate the Right and Up vector
    this->m_vRight = glm::normalize(
            glm::cross(this->m_vFront,
                       this->m_vWorldUp)); // Normalize the vectors, because their length gets closer to 0 the more you look up or down which results in slower movement.
    this->m_vUp = glm::normalize(glm::cross(this->m_vRight, this->m_vFront));
}
