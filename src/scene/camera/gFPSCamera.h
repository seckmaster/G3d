#ifndef SRC_CAMERA_FPSCAMERA_H_
#define SRC_CAMERA_FPSCAMERA_H_

#include "gCamera.h"
#include "../gAABB.h"

class gFPSCamera : public gCamera {
public:
    gFPSCamera(const glm::vec3 &position);

    void update(float deltaTimeS);

    void addData(void *data);

    inline void setClip(bool c) {
        m_bNoClip = c;
        if (c) {
            m_fPreviousVel = m_fMovementSpeed;
            m_fMovementSpeed *= 3;
        } else
            m_fMovementSpeed = m_fPreviousVel;
    }

    inline bool isOnGround() {
        return m_bOnGround;
    }

    inline const gAABB *getAABB() {
        return m_pAABB;
    }

private:
    float m_fYoffset;
    float m_fGround;
    bool m_bOnGround;
    bool m_bNoClip; // act like spectator camera if true
    float m_fPreviousVel;

    gAABB *m_pAABB;
};

#endif /* SRC_CAMERA_FPSCAMERA_H_ */
