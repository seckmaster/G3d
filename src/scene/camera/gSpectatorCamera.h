
#ifndef SRC_CAMERA_SPECTATORCAMERA_H_
#define SRC_CAMERA_SPECTATORCAMERA_H_

#include "gCamera.h"

class gSpectatorCamera : public gCamera {
public:
    gSpectatorCamera(glm::vec3 position);

    void update(float deltaTimeMS);

    void addData(void *data);

private:
};

#endif /* SRC_CAMERA_SPECTATORCAMERA_H_ */
