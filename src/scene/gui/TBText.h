//
//#ifndef TBTEXT_H_
//#define TBTEXT_H_
//
//#include <string>
//#include "../libdrawtext-0.1/src/drawtext.h"
//
//typedef dtx_font* t_font;
//
//class gText : public TBEntity {
//	public:
//		gText(float x, float y, float z, float width, uint32 tex, float angle, const TBColor3f& c, t_font font);
//		~gText();
//
//		void Render(double time, Camera* camera) const;
//		void Update(double time);
//		bool MouseOver(float x, float y);
//
//		void setText(const std::string& t);
//
//	private:
//		std::string 	text;
//		float			angle;
//		TBColor3f 		color;
//		t_font 			font;
//};
//
//#endif /* TBTEXT_H_ */
