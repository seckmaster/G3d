//#include "gButton.h"
//
//gButton::gButton(const glm::vec3& position, const glm::vec2& size,
//		const std::string& texture) :
//		gEntity(position, size, texture) {
//	m_oShader.attachNewShader("data/Shaders/test.vs", "data/Shaders/sprite.frag");
//
//	static const GLfloat vertices[] = {
//        // Pos      // Tex
//        0.0f, 1.0f, 0.0f, 1.0f,
//        1.0f, 0.0f, 1.0f, 0.0f,
//        0.0f, 0.0f, 0.0f, 0.0f,
//
//        0.0f, 1.0f, 0.0f, 1.0f,
//        1.0f, 1.0f, 1.0f, 1.0f,
//        1.0f, 0.0f, 1.0f, 0.0f
//    };
//
//    glGenVertexArrays(1, &VAO);
//    glGenBuffers(1, &VBO);
//
//    glBindBuffer(GL_ARRAY_BUFFER, VBO);
//    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
//
//    glBindVertexArray(this->VAO);
//    glEnableVertexAttribArray(0);
//    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (GLvoid*)0);
//    glBindBuffer(GL_ARRAY_BUFFER, 0);
//
//    glBindVertexArray(0);
//}
//
//bool gButton::MouseOver(float px, float py) {
//	float x = m_vPosition.x;
//	float y = m_vPosition.y;
//	return ((px >= x) && px <= x && py >= y && py <= y);
//}
//
//void gButton::Update(float deltaTimeMS) {
//}
//
//void gButton::Render() const {
//	glDisable(GL_CULL_FACE);
//    glUseProgram(m_oShader.Program);
//    glm::mat4 model;
//    model = glm::translate(model, m_vPosition);
//
//    model = glm::translate(model, glm::vec3(0.5f * m_vSize.x, 0.5f * m_vSize.y, 0.0f));
////    model = glm::rotate(model, rotate, glm::vec3(0.0f, 0.0f, 1.0f));
//    model = glm::translate(model, glm::vec3(-0.5f * m_vSize.x, -0.5f * m_vSize.y, 0.0f));
//    model = glm::scale(model, glm::vec3(m_vSize.x, m_vSize.y, 1.0f));
//
//	GLuint loc = glGetUniformLocation(m_oShader.Program, "model");
//	glUniformMatrix4fv(loc, 1, GL_FALSE, glm::value_ptr(model));
//	loc = glGetUniformLocation(m_oShader.Program, "view");
//	glUniformMatrix4fv(loc, 1, GL_FALSE, glm::value_ptr(m_mView));
//	loc = glGetUniformLocation(m_oShader.Program, "projection");
//	glUniformMatrix4fv(loc, 1, GL_FALSE, glm::value_ptr(m_mProjection));
//
//	loc = glGetUniformLocation(m_oShader.Program, "spriteColor");
//	glUniform3fv(loc, 1, glm::value_ptr(m_vColor));
//
//    glActiveTexture(GL_TEXTURE0);
//    glBindTexture(GL_TEXTURE_2D, tex);
//
//    glBindVertexArray(VAO);
//    glDrawArrays(GL_TRIANGLES, 0, 6);
//    glBindVertexArray(0);
//	glEnable(GL_CULL_FACE);
//}
//
