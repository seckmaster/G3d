//#ifndef ENTITY_H_
//#define ENTITY_H_
//
//#include <string>
//#include <vector>
//#include <glm.hpp>
//#include <OpenGL/gl3.h>
//
//class gEntity : gSprite {
//	public:
//		gEntity(const glm::vec3& pos, const glm::vec2& size,
//				const std::string& texture);
//		virtual ~gEntity();
//
//		virtual void Render() const = 0;
//		virtual void Update(float deltaTimeMS) = 0;
//		virtual bool MouseOver(float x, float y) = 0;
//
//		void setTexCoordinates(const std::vector<float>& coord);
//
//		inline void addAnimator(gAnimator* a) {
//			m_vAnimators.push_back(a);
//		}
//
//		inline void setOpacity(int o) {
//			m_iOpacity = o;
//		}
//
//		inline void setVisible(bool b) {
//			m_bVisible = b;
//		}
//
//		inline bool getVisible() {
//			return m_bVisible;
//		}
//
//	protected:
//		int m_iOpacity;
//		uint32 m_iTex;
//		float m_aCoords[8];
//		bool m_bVisible;
//		std::vector<gAnimator*> m_vAnimators;
//};
//
//#endif /* ENTITY_H_ */
