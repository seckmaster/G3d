//#ifndef SRC_SCENE_GTEXT_H_
//#define SRC_SCENE_GTEXT_H_
//
//#include "gANode.h"
//#include <map>
//
//struct Character {
//		GLuint texture;
//		glm::ivec2 size;
//		glm::ivec2 bearing;
//		GLuint next;
//};
//
//class gText: gANode {
//	public:
//		gText(const glm::vec3& position, int px, const std::string& font, const std::string& text);
//
//		void render() const;
//		void update(float deltaTimeMS);
//
//		inline void setColor(const glm::vec3& color) {
//			m_vColor = color;
//		}
//
//	private:
//		std::string m_sText;
//		GLuint m_iVBO, m_iVAO;
//		int m_iFontSize;
//
//	    std::map<GLchar, Character> Characters;
//
//		glm::vec3 m_vColor;
//};
//
//#endif /* SRC_SCENE_GTEXT_H_ */
