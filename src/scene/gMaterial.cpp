#include "gMaterial.h"

gMaterial gMaterial::EMERALD(vec3(0.0215, 0.1745, 0.0215),
                             vec3(0.07568, 0.61424, 0.07568), vec3(0.633, 0.727811, 0.633), 77);

gMaterial gMaterial::CHROME(vec3(0.25, 0.25, 0.25), vec3(0.4, 0.4, 0.4),
                            vec3(0.774597, 0.774597, 0.774597), 77);

gMaterial gMaterial::RED_PLASTIC(vec3(0, 0, 0), vec3(0.5, 0, 0),
                                 vec3(0.7, 0.6, 0.6), 32);

gMaterial gMaterial::BRONZE(vec3(0.2125, 0.1275, 0.054),
                            vec3(0.714, 0.4284, 0.18144), vec3(0.393548, 0.271906, 0.166721), 26);

gMaterial gMaterial::OBSIDIAN(vec3(0.05375, 0.05, 0.06625),
                              vec3(0.18275, 0.17, 0.22525), vec3(0.332741, 0.328634, 0.346435), 40);

gMaterial gMaterial::BRASS(vec3(0.329412, 0.223529, 0.027451),
                           vec3(0.780392, 0.568627, 0.113725), vec3(0.992157, 0.941176, 0.807843),
                           28);

gMaterial gMaterial::RUBY(vec3(0.1745, 0.01175, 0.01175),
                          vec3(0.61424, 0.04136, 0.04136), vec3(0.727811, 0.626959, 0.626959),
                          0.6);

gMaterial gMaterial::WHITE_RUBBER(vec3(0.05f, 0.05f, 0.05f),
                                  vec3(0.5f, 0.5f, 0.5f), vec3(0.7f, 0.7f, 0.7f), 10);
