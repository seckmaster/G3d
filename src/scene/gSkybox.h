
#ifndef SRC_SCENE_GSKYBOX_H_
#define SRC_SCENE_GSKYBOX_H_

#include "gSceneNode.h"

class gSkybox : public gSceneNode {
public:
    gSkybox(const std::vector<std::string> &textures);

    void render(gShader *shader) const;

    void render() const;

    void update(float deltaTimeMS);

    inline GLuint getCubeMap() {
        return m_iCubeMap;
    }

private:
    GLuint m_iCubeMap;

};

#endif /* SRC_SCENE_GSKYBOX_H_ */
