#include "gAABB.h"

bool gAABB::checkCollision(const gAABB *other, glm::vec3 &normal,
                           float &dcoll) const {
    if (other->position == position)
        return false;
    if (size == glm::vec3() || other->size == glm::vec3())
        return false;

    glm::vec3 tHs = size / glm::vec3(2, 2, 2);
    glm::vec3 oHs = other->size / glm::vec3(2, 2, 2);

    glm::vec3 mina = position - tHs;
    glm::vec3 maxa = position + tHs;
    glm::vec3 minb = other->position - oHs;
    glm::vec3 maxb = other->position + oHs;

//	if (glm::abs(position.x - other.position.x) > (tHs.x + oHs.x))
//		return false;
//
//	if (glm::abs(position.y - other.position.y) > (tHs.y + oHs.y))
//		return false;
//
//	if (glm::abs(position.z - other.position.z) > (tHs.z + oHs.z))
//		return false;
//
//	return true;

    // the normal of each face.
    static const glm::vec3 faces[6] = {
            glm::vec3(-1, 0, 0), // 'left' face normal (-x direction)
            glm::vec3(1, 0, 0), // 'right' face normal (+x direction)
            glm::vec3(0, -1, 0), // 'bottom' face normal (-y direction)
            glm::vec3(0, 1, 0), // 'top' face normal (+y direction)
            glm::vec3(0, 0, -1), // 'far' face normal (-z direction)
            glm::vec3(0, 0, 1), // 'near' face normal (+x direction)
    };

    // distance of collided box to the face.
    float distances[6] = {
            (maxb.x - mina.x), // distance of box 'b' to face on 'left' side of 'a'.
            (maxa.x - minb.x), // distance of box 'b' to face on 'right' side of 'a'.
            (maxb.y - mina.y), // distance of box 'b' to face on 'bottom' side of 'a'.
            (maxa.y - minb.y), // distance of box 'b' to face on 'top' side of 'a'.
            (maxb.z - mina.z), // distance of box 'b' to face on 'far' side of 'a'.
            (maxa.z - minb.z), // distance of box 'b' to face on 'near' side of 'a'.
    };

    // scan each face, make sure the box intersects,
    // and take the face with least amount of intersection
    // as the collided face.
//	dcoll = distances[0];
    for (int i = 0; i < 6; i++) {
        // box does not intersect face. So boxes don't intersect at all.
        if (distances[i] < 0.0f)
            return false;

        // face of least intersection depth. That's our candidate.
        if ((i == 0) || (distances[i] < dcoll)) {
            normal = faces[i];
            dcoll = distances[i];
        }
    }

    return true;
}
