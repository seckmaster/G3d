#ifndef SRC_GMATERIAL_H_
#define SRC_GMATERIAL_H_

#include <glm/glm.hpp>
#include <OpenGL/gl3.h>

using namespace glm;

class gMaterial {
public:
    gMaterial() :
            ambient(1.0, 1.0, 1.0), diffuse(1.0, 1.0, 1.0), specular(0.5f,
                                                                     0.5f, 0.5f), shininess(32) {
    }

    gMaterial(const glm::vec3 &ambient, const glm::vec3 &diffuse,
              const glm::vec3 &specular, GLfloat shininess) :
            ambient(ambient), diffuse(diffuse), specular(specular), shininess(
            shininess) {
    }

    gMaterial(const gMaterial &o) {
        this->ambient = o.ambient;
        this->diffuse = o.diffuse;
        this->specular = o.specular;
        this->shininess = o.shininess;
    }

    gMaterial &operator=(const gMaterial &o) {
        this->ambient = o.ambient;
        this->diffuse = o.diffuse;
        this->specular = o.specular;
        this->shininess = o.shininess;
        return *this;
    }

    glm::vec3 ambient;
    glm::vec3 diffuse;
    glm::vec3 specular;
    GLfloat shininess;

public:
    static gMaterial EMERALD;
    static gMaterial CHROME;
    static gMaterial RED_PLASTIC;
    static gMaterial BRONZE;
    static gMaterial OBSIDIAN;
    static gMaterial BRASS;
    static gMaterial RUBY;
    static gMaterial WHITE_RUBBER;
};

#endif /* SRC_GMATERIAL_H_ */
