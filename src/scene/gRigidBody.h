#ifndef SRC_SCENE_GRIGIDBODY_H_
#define SRC_SCENE_GRIGIDBODY_H_

#include "gAABB.h"
#include "gSceneNode.h"

class gRigidBody : public gSceneNode {
public:
    gRigidBody(gAABB *shape);

    virtual ~gRigidBody();

    void render() const;

    void render(gShader *shader) const;

    void update(float deltaTimeMs);

    void setBodyPosition(const glm::vec3 &position) {
        setPosition(position);
        m_pAABB->setPosition(position);
    }

    inline const gMaterial &getMaterial() {
        return m_oMaterial;
    }

    inline void setMaterial(const gMaterial &m) {
        m_oMaterial = m;
    }

    inline void setMaterial(const glm::vec3 &ambient,
                            const glm::vec3 &diffuse, const glm::vec3 &specular,
                            GLfloat shininess) {
        m_oMaterial.ambient = ambient;
        m_oMaterial.diffuse = diffuse;
        m_oMaterial.specular = specular;
        m_oMaterial.shininess = shininess;
    }

    inline void enableNormalMapping() {
        m_bNormalMapping = true;
    }

    inline void disableNormalMapping() {
        m_bNormalMapping = false;
    }

    inline void enableParallaxMapping() {
        m_bParallaxMapping = true;
    }

    inline void disableParallaxMapping() {
        m_bParallaxMapping = false;
    }

    inline const gAABB *getAABB() {
        return m_pAABB;
    }

    inline void setMass(int mass) {
        m_iMass = mass;
    }

    inline int getMass() {
        return m_iMass;
    }

private:
    gMaterial m_oMaterial;
    bool m_bNormalMapping;
    bool m_bParallaxMapping;

    gAABB *m_pAABB;
    int m_iMass; // 0 = inf
};

#endif /* SRC_SCENE_GRIGIDBODY_H_ */
