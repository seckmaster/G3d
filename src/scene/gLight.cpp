#include "gLight.h"

#include "animator/gAnimator.h"

gShadowInfo::gShadowInfo() {
    m_vSize = glm::vec2(1024, 1024);
    m_iFBO = 0;
}

gLight::gLight() {
    m_oLight.ambient = glm::vec3(0.2, 0.2, 0.2);
    m_oLight.diffuse = glm::vec3(0.5, 0.5, 0.5);
    m_oLight.specular = glm::vec3(1, 1, 1);
    m_oLight.constant = 1;
    m_oLight.linear = 0.14;
    m_oLight.qudratic = 0.07;
    m_oLight.cutOff = 12.0f;
    m_oLight.outerCutOff = 20.0f;
    m_eType = ELT_DIRECTIONAL;
    m_bEnabled = true;
    m_pShadowInfo = NULL;
}

gLight::gLight(const glm::vec3 &position, const glm::vec3 &rotation, E_LIGHT_TYPE type) :
        gSceneNode() {
    m_oLight.ambient = glm::vec3(0.2, 0.2, 0.2);
    m_oLight.diffuse = glm::vec3(0.5, 0.5, 0.5);
    m_oLight.specular = glm::vec3(1, 1, 1);
    m_oLight.constant = 1;
    m_oLight.linear = 0.14;
    m_oLight.qudratic = 0.07;
    m_oLight.cutOff = 12.0f;
    m_oLight.outerCutOff = 20.0f;
    setSize(1, 1, 1);
    m_bEnabled = true;
    m_eType = type;
    m_pShadowInfo = NULL;

    setPosition(position);
    setRotation(rotation);
}

gLight::gLight(const Light &light, const glm::vec3 &position,
               const glm::vec3 &rotation, E_LIGHT_TYPE type) :
        gSceneNode() {
    m_oLight.ambient = light.ambient;
    m_oLight.diffuse = light.diffuse;
    m_oLight.specular = light.specular;
    m_oLight.constant = light.constant;
    m_oLight.linear = light.linear;
    m_oLight.qudratic = light.qudratic;
    m_oLight.cutOff = light.cutOff;
    m_oLight.outerCutOff = light.outerCutOff;
    m_eType = type;

    setPosition(position);
    setRotation(rotation);
    setSize(1, 1, 1);
    m_bEnabled = true;
    m_pShadowInfo = NULL;
}

void gLight::render() const {
    render(m_oShader);
}

void gLight::render(gShader *shader) const {
    if (!m_bVisible)
        return;

    glm::mat4 model;
    model = glm::translate(model, getPosition());
    model = glm::rotate(model, getRotation().x, glm::vec3(1.0f, 0.0f, 0.0f));
    model = glm::rotate(model, getRotation().y, glm::vec3(0.0f, 1.0f, 0.0f));
    model = glm::rotate(model, getRotation().z, glm::vec3(0.0f, 0.0f, 1.0f));
    model = glm::scale(model, getSize());

    m_oShader->useProgram();
    m_oShader->setMatrix4f("model", model);
    m_oShader->setMatrix4f("view", m_mView);
    m_oShader->setMatrix4f("projection", m_mProjection);
    m_oShader->setVector3f("mat.ambient", m_oLight.ambient);

    if (m_pModel)
        m_pModel->Draw(*shader);
}

void gLight::update(float deltaTimeMS) {
    std::vector<gAnimator *>::const_iterator it = m_vAnimators.begin();
    for (; it != m_vAnimators.end(); ++it) {
        (*it)->animateNode(this, deltaTimeMS);
    }
}

void gLight::createShadowInfo() {
    GLuint fbo;
    glGenFramebuffers(1, &fbo);
    const GLuint SHADOW_WIDTH = 1024, SHADOW_HEIGHT = 1024;

    GLuint depthMap;
    glGenTextures(1, &depthMap);
    glBindTexture(GL_TEXTURE_2D, depthMap);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, SHADOW_WIDTH,
                 SHADOW_HEIGHT, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
    GLfloat borderColor[] = {1.0, 1.0, 1.0, 1.0};
    glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);

    glBindFramebuffer(GL_FRAMEBUFFER, fbo);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D,
                           depthMap, 0);
    glDrawBuffer(GL_NONE);
    glReadBuffer(GL_NONE);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    m_pShadowInfo = new gShadowInfo();
    m_pShadowInfo->setFBO(fbo);
    m_pShadowInfo->setShadowMap(depthMap);
}
