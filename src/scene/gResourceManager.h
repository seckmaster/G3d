#ifndef SRC_GRESOURCEMANAGER_H_
#define SRC_GRESOURCEMANAGER_H_

#include "shader/gShader.h"
#include "model/gModel3D.h"
#include <string>
#include <map>

#include "assimp/Importer.hpp"  //OO version Header!
#include "assimp/postprocess.h"
#include "assimp/scene.h"

#include "model/gMesh.h"

class gResourceManager {
public:
    gResourceManager();

    GLuint loadTexture(const std::string &path, const std::string &name, bool prefix = true);

    unsigned char *loadRawTexture(const std::string &path, int &width,
                                  int &height, bool prefix = true);

    gShader* loadShader(const std::string &vertexPath,
                        const std::string &fragmentPath,
                        const std::string &name);

    gModel3D* load3dModel(const std::string &path,
                          const std::string &name,
                          int params = 0);

    inline gShader* getShader(const std::string &name) {
        return m_dictShaders[name];
    }

    inline gModel3D* getModel(const std::string &name) {
        return m_dictModels[name];
    }

private:
    std::map<std::string, GLuint> m_dictTextures;
    std::map<std::string, gShader*> m_dictShaders;
    std::map<std::string, gModel3D*> m_dictModels;

//		std::ostream* m_pStream;

private:
    GLuint loadGLTexture(const std::string &path);

    void processNode(gModel3D *model, aiNode *node, const aiScene *scene);

    gMesh processMesh(gModel3D *model, aiMesh *mesh, const aiScene *scene);

    std::vector<Texture> loadMaterialTextures(const std::string &directory,
                                              aiMaterial *mat, aiTextureType type,
                                              const std::string &typeName);
};

#endif /* SRC_GRESOURCEMANAGER_H_ */
