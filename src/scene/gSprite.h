#ifndef SRC_SCENE_GSPRITE_H_
#define SRC_SCENE_GSPRITE_H_

#include "gSceneNode.h"
#include "animator/gAnimator.h"
#include "camera/gCamera.h"

class gSprite : public gSceneNode {
public:
    gSprite(const glm::vec3 &pos, const glm::vec2 &size);

    void render() const;

    void render(gShader *shader) const;

    void update(float deltaTimeS);

    inline void setSize(const glm::vec2 &size) {
        gSceneNode::setSize(size.x, size.y, 0);
    }

    inline void setColor(const glm::vec3 &color) {
        m_vColor = color;
    }

    inline void setTexture(GLuint t) {
        tex = t;
    }

    inline void enableTextureAnimation() {
        m_bTextureAnimation = true;
    }

    inline void disableTextureAnimation() {
        m_bTextureAnimation = false;
    }

    inline bool isTextureAnimationEnabled() {
        return m_bTextureAnimation;
    }

private:
    GLuint VAO, VBO, tex;
    bool m_bTextureAnimation;

    glm::vec3 m_vColor;
};

#endif /* SRC_SCENE_GSPRITE_H_ */
