
#ifndef SRC_SCENE_GSOUND_H_
#define SRC_SCENE_GSOUND_H_

#include <irrklang/irrKlang.h>

#if defined(WIN32)
#include <conio.h>
#else

#include <irrklang/conio.h>

#endif

#include "gActor.h"

enum E_SOUND_TYPE {
    EST_AMBIENT,
    EST_POSITIONAL
};

class gSound : public gActor {
public:
    gSound(irrklang::ISoundEngine *engine, const std::string &path, bool loop);

    gSound(irrklang::ISoundEngine *engine, const glm::vec3 &position, const std::string &path, bool loop);

    void play();

    void stop();

    void setSoundEngine(irrklang::ISoundEngine *engine) { m_pEngine = engine; }

    void setType(E_SOUND_TYPE type) { m_eType = type; }

private:
    irrklang::ISoundSource *m_pSound;
    irrklang::ISound *m_pSnd;
    std::string m_sFile;

    irrklang::ISoundEngine *m_pEngine;
    bool m_bLoop;
    E_SOUND_TYPE m_eType;
};

#endif /* SRC_SCENE_GSOUND_H_ */
