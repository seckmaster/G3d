#ifndef SRC_SCENE_GLIGHT_H_
#define SRC_SCENE_GLIGHT_H_

#include "gSceneNode.h"

#define MAX_LIGHTS 10

enum E_LIGHT_TYPE {
    /* 0 */ ELT_POINT,
    /* 1 */ ELT_DIRECTIONAL,
    /* 2 */ ELT_SPOTLIGHT
};

typedef struct {
    glm::vec3 ambient;
    glm::vec3 diffuse;
    glm::vec3 specular;

    float constant;
    float linear;
    float qudratic;

    glm::vec3 direction;
    float cutOff;
    float outerCutOff;
} Light;

class gShadowInfo {
public:
    gShadowInfo();

    inline glm::mat4 getProjection() {
        return m_mProjection;
    }

    inline void setProjection(const glm::mat4 &proj) {
        m_mProjection = proj;
    }

    inline void setFBO(GLuint fbo) {
        m_iFBO = fbo;
    }

    inline GLuint getFBO() {
        return m_iFBO;
    }

    inline const glm::vec2 &getSize() {
        return m_vSize;
    }

    inline void setShadowMap(GLuint t) {
        m_iShadowMap = t;
    }

    inline GLuint getShadowMap() {
        return m_iShadowMap;
    }

private:
    glm::mat4 m_mProjection;
    glm::vec2 m_vSize;
    GLuint m_iFBO;
    GLuint m_iShadowMap;
};

class gLight : public gSceneNode {
public:
    gLight();

    gLight(const glm::vec3 &position, const glm::vec3 &rotation,
           E_LIGHT_TYPE type = ELT_POINT);

    gLight(const Light &light, const glm::vec3 &position, const glm::vec3 &rotation,
           E_LIGHT_TYPE type = ELT_POINT);

    void render(gShader *shader) const;

    void render() const;

    void update(float deltaTimeMS);

    inline const Light getLight() {
        return m_oLight;
    }

    inline void setLight(const glm::vec3 &ambient, const glm::vec3 &diffuse,
                         const glm::vec3 &specular) {
        m_oLight.ambient = ambient;
        m_oLight.diffuse = diffuse;
        m_oLight.specular = specular;
    }

    inline void setAttenuation(float c, float l, float q) {
        m_oLight.constant = c;
        m_oLight.linear = l;
        m_oLight.qudratic = q;
    }

    inline void setSpotlight(float cutOff, float outerCutOff) {
        m_oLight.cutOff = cutOff;
        m_oLight.outerCutOff = outerCutOff;
    }

    inline int getType() {
        return m_eType;
    }

    inline void enable() {
        m_bEnabled = true;
    }

    inline void disable() {
        m_bEnabled = false;
    }

    inline void switchLight() {
        m_bEnabled = !m_bEnabled;
    }

    inline bool isEnabled() {
        return m_bEnabled;
    }

    inline void setShadowInfo(gShadowInfo *shadowInfo) {
        m_pShadowInfo = shadowInfo;
    }

    inline gShadowInfo *getShadowInfo() {
        return m_pShadowInfo;
    }

    void createShadowInfo();

private:
    Light m_oLight;
    E_LIGHT_TYPE m_eType;
    bool m_bEnabled;

    // shadow info
    gShadowInfo *m_pShadowInfo;
};

#endif /* SRC_SCENE_GLIGHT_H_ */
