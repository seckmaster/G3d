#include "gSceneNode.h"

gSceneNode::gSceneNode() {
    m_iDiffuseTexture = -1;
    m_iSpecularTexture = -1;
    m_iNormalTexture = -1;
    m_iDisplacmentTexture = -1;
    m_pModel = NULL;
    m_bVisible = true;
    m_bDrawable = true;
}

gSceneNode::~gSceneNode() {
    for (std::vector<gSceneNode *>::iterator it = m_vChildren.begin();
         it != m_vChildren.end(); it++) {
        delete (*it);
    }
    m_vChildren.clear();
}
