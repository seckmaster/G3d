#ifndef SRC_WORLD_SCENEMANAGER_
#define SRC_WORLD_SCENEMANAGER_

#include <vector>
#include <glm/glm.hpp>
#include <string>
#include <GLFW/glfw3.h>

#include "gAABB.h"
#include "gResourceManager.h"
#include "shader/gShader.h"
#include "gSkybox.h"
#include "particle/gParticle.h"
#include "animator/gPath.h"
#include "animator/gARotationAnimator.h"
#include "animator/gPathAnimator.h"
#include "animator/gTextureAnimator.h"
#include "animator/gFadeAnimator.h"
#include "camera/gCamera.h"
#include "camera/gFPSCamera.h"
#include "camera/gSpectatorCamera.h"
#include "camera/gStaticCamera.h"
#include "model/gModel3D.h"
#include "gLight.h"
#include "gRigidBody.h"
#include "gSceneNode.h"
#include "gSprite.h"
#include "gSound.h"

class gSceneManager {
public:
    gSceneManager(int width, int height, GLFWwindow *window);

    void drawScene();

    void updateScene(float deltaTimeMS);

    inline gResourceManager *getResourceManager() {
        return m_pResourceMngr;
    }

    inline gCamera *getSelectedCamera() {
        return m_pSelectedCamera;
    }

    gCamera *addFPSCamera(glm::vec3 pos, glm::vec3 rotation);

    gCamera *addStaticCamera(const glm::vec3 &pos,
                             const glm::vec3 &rotation);

    gCamera *addSpectatorCamera(const glm::vec3 &pos,
                                const glm::vec3 &rotation);

    gLight *addLightSource(const glm::vec3 &position,
                           const glm::vec3 &rotation, E_LIGHT_TYPE type);

    void addRigidBody(gRigidBody *body);

    gRigidBody *addRigidBody(const glm::vec3 &position,
                             const glm::vec3 &size, const glm::vec3 &aabbSize, int mass = 0);

    gSprite *addSprite(const glm::vec3 &position, const glm::vec2 &size);

    gSkybox *createSkybox(const std::string &path, const std::string &ext);

    gARotationAnimator *createRotationAnimator(const glm::vec3 &direction,
                                               const glm::vec3 &speed);

    gPathAnimator *createPathAnimator(E_PATH_TYPE type, float step);

    gSound *createSound(const std::string &path, bool loop,
                        E_SOUND_TYPE type);

    gCamera *selectCamera(int i);

public:
    // default event handlers
    void handleKeyPres(int key, int code, int action, int mods);

    void handleMousePress(int key, int action, int mods);

    void handleMouseMove(double x, double y);

    void setKeyPressHandler(
            void (*fn)(int key, int code, int action, int mods));

    void setMousePressHandler(void (*fn)(int key, int action, int mods));

    void setMouseMoveHandler(void (*fn)(double x, double y));

    void setCollisionEventHandler(void (*fn)(gRigidBody *a, gRigidBody *b));

private:
    // user specified event handlers
    void (*m_fnUserHandleKeyPress)(int key, int code, int action, int mods);

    void (*m_fnUserHandleMousePress)(int key, int action, int mods);

    void (*m_fnUserHandleMouseMove)(double x, double y);

    void (*m_fnCollisionEventHandler)(gRigidBody *a, gRigidBody *b);

    double m_fMouse_x, m_fMouse_y;
    GLFWwindow *m_pWindow;

private:
    // containers for objects
    std::vector<gRigidBody *> m_vSceneContainer;
    std::vector<gCamera *> m_vCameraContainer;
    std::vector<gLight *> m_vLightContainer;
    std::vector<gSprite *> m_vSpriteContainer;
    std::vector<gSound *> m_vSoundContainer;
    std::vector<gParticleGenerator *> m_vParticleSystems;
    std::vector<gAABB *> m_vCollisionShapes;

public:
    gRigidBody *weapon; // TODO: - remove

private:
    // resource mngr
    gResourceManager *m_pResourceMngr;
    // pointer to a currently selected camera
    gCamera *m_pSelectedCamera;
    // skybox
    gSkybox *m_pSkybox;
    // projection matrix
    glm::mat4 m_mProjection;
    // sound engine
    irrklang::ISoundEngine *m_pSoundEngine;

    // shader for depth textures
    gShader *m_oShadow;

    int m_iWidth, m_iHeight;

private:
    /**
     * FBO holder for texture rendering.
     */
    GLuint m_iMainFBO;
    GLuint m_iBloomTex;
    GLuint m_iDepthFBO;
    GLuint m_iDepthMap;
    gSprite *m_pFrameBufferSprite;
    gSprite *m_pShadowSprite;
    // post-processing
    bool m_bInversion;
    bool m_bGrayscale;
    bool m_bKernel;
    bool m_bBlur;
    bool m_bEdgeDetection;
    bool m_bSSAO;
    bool m_bBloom;

public:
    enum E_EFECT_TYPE {
        EET_INVERSION,
        EET_GRAYSCALE,
        EET_KERNEL,
        EET_BLUR,
        EET_EDGEDETECTION,
        EET_SSAO,
        EET_BLOOM
    };

    inline void switchPostProcessingEffect(E_EFECT_TYPE effect) {
        switch (effect) {
            case EET_INVERSION:
                m_bInversion = !m_bInversion;
                break;
            case EET_GRAYSCALE:
                m_bGrayscale = !m_bGrayscale;
                break;
            case EET_KERNEL:
                m_bKernel = !m_bKernel;
                break;
            case EET_BLUR:
                m_bBlur = !m_bBlur;
                break;
            case EET_EDGEDETECTION:
                m_bEdgeDetection = !m_bEdgeDetection;
                break;
            case EET_SSAO:
                m_bSSAO = !m_bSSAO;
                break;
            case EET_BLOOM:
                m_bBloom = !m_bBloom;
                break;
        }
    }

private:
    void setupRC();
//		bool
};

#endif /* SRC_WORLD_SCENEMANAGER_ */
