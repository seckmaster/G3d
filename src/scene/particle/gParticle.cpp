
#include "gParticle.h"

gParticleGenerator::gParticleGenerator(int n) {
    m_iNumOfParticles = n;
    for (int i = 0; i < n; i++)
        m_vParticles.push_back(gParticle());
}

void gParticleGenerator::render() {
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texture);
    for (int i = 0; i < m_iNumOfParticles; i++) {
        m_vParticles.at(i).render();
    }
}
