#ifndef SRC_GPARTICLE_H_
#define SRC_GPARTICLE_H_

#include "../gSprite.h"

class gParticle : public gSprite {
public:
    GLfloat lifespan, age, scale, direction;
    glm::vec3 movement;
    glm::vec3 pull;

    gParticle() : gSprite(glm::vec3(), glm::vec2(5, 5)) {}
};

class gParticleGenerator {
public:
    gParticleGenerator(int n);

    virtual ~gParticleGenerator() {
    }

    void render();

    virtual void update(float deltaTimeMS) = 0;

    inline void setProjection(const glm::mat4 &projection) {
        m_mProjection = projection;
    }

    inline void setView(const glm::mat4 &view) {
        m_mView = view;
    }

protected:
    int m_iNumOfParticles;
    std::vector<gParticle> m_vParticles;
    glm::vec3 systemPull;
    GLuint texture;

    glm::mat4 m_mProjection;
    glm::mat4 m_mView;
};

#endif /* SRC_GPARTICLE_H_ */
