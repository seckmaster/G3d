#include "gShader.h"
#include <iostream>

void gShader::setMatrix4f(const std::string &name, const glm::mat4 &val) {
    GLuint loc = getLocationForName(name);
    glUniformMatrix4fv(loc, 1, GL_FALSE, glm::value_ptr(val));
}

void gShader::setVector3f(const std::string &name, const glm::vec3 &val) {
    GLuint loc = getLocationForName(name);
    glUniform3fv(loc, 1, glm::value_ptr(val));
}

void gShader::setVector2f(const std::string &name, const glm::vec2 &val) {
    GLuint loc = getLocationForName(name);
    glUniform3fv(loc, 1, glm::value_ptr(val));
}

void gShader::setFloat1f(const std::string &name, float val) {
    GLuint loc = getLocationForName(name);
    glUniform1f(loc, val);
}

void gShader::setInteger1i(const std::string &name, int val) {
    GLuint loc = getLocationForName(name);
    glUniform1i(loc, val);
}

GLuint gShader::getLocationForName(const std::string &name) {
    auto it = cache.find(name);
    if (it != cache.end()) {
        return it->second;
    }
    GLuint loc = glGetUniformLocation(programID, name.c_str());
    cache[name] = loc;
    return loc;
}
