/*
 * gShader.h
 *
 *  Created on: Oct 29, 2015
 *      Author: toni
 */

#ifndef SRC_SHADER_GSHADER_H_
#define SRC_SHADER_GSHADER_H_

#include <string>
#include <OpenGL/gl3.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/std_based_type.hpp>
#include <glm/gtx/vector_angle.hpp>
#include <glm/gtx/compatibility.hpp>
#include <glm/gtx/matrix_operation.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/euler_angles.hpp>
#include <glm/ext.hpp>
#include <map>

class gShader {
public:
    gShader(const std::string& shaderName, GLuint programID) : name(shaderName), programID(programID) {
    }

    inline void useProgram() const {
        glUseProgram(programID);
    }

    inline GLuint getProgramId() {
        return programID;
    }

    void setMatrix4f(const std::string &name, const glm::mat4 &val);

    void setVector3f(const std::string &name, const glm::vec3 &val);

    void setVector2f(const std::string &name, const glm::vec2 &val);

    void setFloat1f(const std::string &name, float val);

    void setInteger1i(const std::string &name, int val);

private:
    const std::string& name;
    const GLuint programID;

    std::map<std::string, GLuint> cache = std::map<std::string, GLuint>();
    GLuint getLocationForName(const std::string &name);
};

#endif /* SRC_SHADER_GSHADER_H_ */
