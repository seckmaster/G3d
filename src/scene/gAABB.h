#ifndef SRC_SCENE_GAABB_H_
#define SRC_SCENE_GAABB_H_

#include <glm/glm.hpp>
#include <cmath>

class gActor;

class gAABB {
public:
    gAABB(const glm::vec3 &position, const glm::vec3 &size) :
            size(size), position(position) {
    }

    bool checkCollision(const gAABB *other, glm::vec3 &normal, float &dcoll) const;

    inline void setSize(const glm::vec3 &size) {
        this->size = size;
    }

    inline void setPosition(const glm::vec3 &position) {
        this->position = position;
    }

private:
    glm::vec3 size;
    glm::vec3 position;
};

#endif /* SRC_SCENE_GAABB_H_ */
