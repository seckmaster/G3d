
#include "gSprite.h"

gSprite::gSprite(const glm::vec3 &pos, const glm::vec2 &size) {
    setPosition(pos);
    setSize(size);
    m_vColor = glm::vec3(1, 1, 1);
    m_bBlending = false;
    m_fOpacity = 1.0f;

//	m_oShader.attachNewShader("data/Shaders/test.vs", "data/Shaders/sprite_depth.frag");
    tex = -1;
    GLfloat vertices[] = {
            // Pos      // Tex
            0.0f, 1.0f, 0.0f, 1.0f,
            1.0f, 0.0f, 1.0f, 0.0f,
            0.0f, 0.0f, 0.0f, 0.0f,

            0.0f, 1.0f, 0.0f, 1.0f,
            1.0f, 1.0f, 1.0f, 1.0f,
            1.0f, 0.0f, 1.0f, 0.0f
    };

    m_bTextureAnimation = false;

    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);

    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    glBindVertexArray(this->VAO);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (GLvoid *) 0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindVertexArray(0);
}

void gSprite::render() const {
    glDisable(GL_CULL_FACE);
    m_oShader->useProgram();

    glm::mat4 model;
    model = glm::translate(model, getPosition() - vec3(getSize().x / 2, getSize().y / 2, 1.0f));
    model = glm::rotate(model, getRotation().x, glm::vec3(1.0f, 0.0f, 0.0f));
    model = glm::rotate(model, getRotation().y, glm::vec3(0.0f, 1.0f, 0.0f));
    model = glm::rotate(model, getRotation().z, glm::vec3(0.0f, 0.0f, 1.0f));
    model = glm::scale(model, glm::vec3(getSize().x, getSize().y, 1.0f));

    m_oShader->setMatrix4f("model", model);
    m_oShader->setMatrix4f("view", m_mView);
    m_oShader->setMatrix4f("projection", m_mProjection);
    m_oShader->setVector3f("spriteColor", m_vColor);
    m_oShader->setInteger1i("textureAnimation", m_bTextureAnimation);
    m_oShader->setInteger1i("blending", m_bBlending);
    m_oShader->setFloat1f("opacity", m_fOpacity);

    if (m_bBlending) {
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    }

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, tex);

    glBindVertexArray(VAO);
    glDrawArrays(GL_TRIANGLES, 0, 6);
    glBindVertexArray(0);
    glBindTexture(GL_TEXTURE_2D, 0);

    glEnable(GL_CULL_FACE);

    if (m_bBlending)
        glDisable(GL_BLEND);
}

void gSprite::render(gShader *shader) const {

}

void gSprite::update(float deltaTimeS) {
    std::vector<gAnimator *>::const_iterator it = m_vAnimators.begin();
    for (; it != m_vAnimators.end(); ++it) {
        (*it)->animateNode(this, deltaTimeS);
    }
}
