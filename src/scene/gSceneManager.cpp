#include "gSceneManager.h"

using namespace std;
using namespace glm;
using namespace irrklang;

gSceneManager::gSceneManager(int width, int height, GLFWwindow *window) :
        m_iWidth(width), m_iHeight(height), m_pWindow(window) {
    m_bInversion = false;
    m_bGrayscale = false;
    m_bKernel = false;
    m_bBlur = false;
    m_bEdgeDetection = false;
    m_bSSAO = false;
    m_bBloom = true;

    m_pSkybox = NULL;
    m_fnUserHandleKeyPress = NULL;
    m_fnUserHandleMouseMove = NULL;
    m_fnUserHandleMousePress = NULL;

    m_pSoundEngine = createIrrKlangDevice();
    m_pResourceMngr = new gResourceManager();

    setupRC();
}

void gSceneManager::drawScene() {
    /**
     *  1. Render to shadow texture.
     */
//	vector<gLight*>::iterator iter = m_vLightContainer.begin();
//	for (; iter != m_vLightContainer.end(); ++iter) {
    gLight *light = m_vLightContainer[0];
    gShadowInfo *shadow = light->getShadowInfo();
    glCullFace(GL_FRONT);
    glViewport(0, 0, shadow->getSize().x, shadow->getSize().y);
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, shadow->getFBO());
    glClear(GL_DEPTH_BUFFER_BIT);

    mat4 lightProjection, lightView;
    GLfloat near_plane = -800.f, far_plane = 800.f;
    GLfloat left = 300;

    lightProjection = ortho(-left, left, -left, left, near_plane, far_plane);
    lightView =
            (new gStaticCamera(light->getPosition(), vec3(0, -90, 0)))->getViewMatrix();
    shadow->setProjection(lightProjection * lightView);

    vector<gRigidBody *>::const_iterator sceneIt = m_vSceneContainer.begin();
    for (; sceneIt != m_vSceneContainer.end(); ++sceneIt) {
        (*sceneIt)->setView(lightView);
        (*sceneIt)->setProjection(lightProjection);
        (*sceneIt)->setCameraPos(m_pSelectedCamera->getPosition());
        (*sceneIt)->render(m_oShadow);
    }
    glCullFace(GL_BACK);

    mat4 view = m_pSelectedCamera->getViewMatrix();
    glViewport(0, 0, m_iWidth, m_iHeight);

    /**
     * 2. Render to depth texture.
     */
    glBindFramebuffer(GL_FRAMEBUFFER, m_iDepthFBO);
    glClear(GL_DEPTH_BUFFER_BIT);
    sceneIt = m_vSceneContainer.begin();
    for (; sceneIt != m_vSceneContainer.end(); ++sceneIt) {
        (*sceneIt)->setView(view);
        mat4 proj = perspective(45.0f, (float) m_iWidth / (float) m_iHeight, 0.1f, 3000.0f);
        (*sceneIt)->setProjection(proj);
        (*sceneIt)->setCameraPos(m_pSelectedCamera->getPosition());
        (*sceneIt)->render(m_oShadow);
    }

    /**
     * 3. Render to custom framebuffer.
     */
    glBindFramebuffer(GL_FRAMEBUFFER, m_iMainFBO);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    if (m_pSkybox) {
        m_pSkybox->setView(view);
        m_pSkybox->render();
    }

    vector<gSprite *>::const_iterator iter2 = m_vSpriteContainer.begin();
    for (; iter2 != m_vSpriteContainer.end(); ++iter2) {
        (*iter2)->setView(view);
        (*iter2)->render();
    }

    vector<gParticleGenerator *>::const_iterator iterator =
            m_vParticleSystems.begin();
    for (; iterator != m_vParticleSystems.end(); ++iterator) {
        (*iterator)->render();
    }

    vector<gLight *>::const_iterator it = m_vLightContainer.begin();
    for (; it != m_vLightContainer.end(); ++it) {
        (*it)->setView(view);
        (*it)->render();
    }

    sceneIt = m_vSceneContainer.begin();
    for (; sceneIt != m_vSceneContainer.end(); ++sceneIt) {
        (*sceneIt)->setProjection(m_mProjection);
        (*sceneIt)->setCameraPos(m_pSelectedCamera->getPosition());
        (*sceneIt)->render();
    }

//	weapon->setPosition(m_pSelectedCamera->getPosition());
//	weapon->render();

    m_pShadowSprite->setTexture(shadow->getShadowMap());
    m_pShadowSprite->setView(view);
    m_pShadowSprite->render();

    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    /**
     * 4. Display final output.
     */
//	m_pFrameBufferSprite->setTexture(shadow->getShadowMap());
    m_pFrameBufferSprite->getShader()->useProgram();
    m_pFrameBufferSprite->getShader()->setInteger1i("blur", m_bBlur);
    m_pFrameBufferSprite->getShader()->setInteger1i("inversion", m_bInversion);
    m_pFrameBufferSprite->getShader()->setInteger1i("grayscale", m_bGrayscale);
    m_pFrameBufferSprite->getShader()->setInteger1i("kernel", m_bKernel);
    m_pFrameBufferSprite->getShader()->setInteger1i("edgedetection", m_bEdgeDetection);
    m_pFrameBufferSprite->getShader()->setInteger1i("blending", false);
    m_pFrameBufferSprite->getShader()->setInteger1i("ssao", m_bSSAO);
    m_pFrameBufferSprite->getShader()->setInteger1i("bloom", /*m_bBloom*/false);

    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, m_iDepthMap);
//	glActiveTexture(GL_TEXTURE2);
//	glBindTexture(GL_TEXTURE_2D, m_iBloomTex);
    m_pFrameBufferSprite->render();
    glBindTexture(GL_TEXTURE_2D, 0);
}

void gSceneManager::updateScene(float deltaTimeS) {
    vector<gRigidBody *>::iterator it = m_vSceneContainer.begin();
    for (; it != m_vSceneContainer.end(); ++it) {
        (*it)->update(deltaTimeS);
    }

    vector<gLight *>::iterator iter = m_vLightContainer.begin();
    for (; iter != m_vLightContainer.end(); ++iter) {
        (*iter)->update(deltaTimeS);
    }

    vector<gParticleGenerator *>::iterator iterator = m_vParticleSystems.begin();
    for (; iterator != m_vParticleSystems.end(); ++iterator) {
        (*iterator)->update(deltaTimeS);
    }

    weapon->update(deltaTimeS);

    /**
     * Handle collisions.
     */
    for (int i = 0; i < m_vSceneContainer.size(); i++) {
        gRigidBody *a = m_vSceneContainer[i];
        if (!a->isDrawable())
            continue;

        vec3 normal;
        float depth;
        for (int j = 0; j < m_vSceneContainer.size(); j++) {
            gRigidBody *b = m_vSceneContainer[j];
            if (i == j || !b->isDrawable())
                continue;

            if (a->getAABB()->checkCollision(b->getAABB(), normal, depth)) {
//				if (a->getMass() + b->getMass() == 0)
//					continue;
//				if (a->getMass() == 0 || a->getMass() > b->getMass()) {
//					vec3 pos = b->getPosition();
//					b->setPosition(pos - depth * normal);
//				} else {
//					vec3 pos = a->getPosition();
//					a->setPosition(pos - depth * normal);
//				}
                if (m_fnCollisionEventHandler)
                    m_fnCollisionEventHandler(a, b);
            }
        }
    }
    vec3 normal;
    float depth;
    gFPSCamera *fps = (gFPSCamera *) m_vCameraContainer[0];
    for (int i = 0; i < m_vSceneContainer.size(); i++) {
        gRigidBody *a = m_vSceneContainer[i];
        if (!a->isDrawable())
            continue;

        if (fps->getAABB()->checkCollision(a->getAABB(), normal, depth)) {
            if (a->getMass() == 0) {
                vec3 pos = fps->getPosition();
                fps->setPosition(pos - depth * normal);
            }
            gRigidBody *cam = new gRigidBody(new gAABB(vec3(), vec3()));
            cam->setUserData(new string("player"));
            m_fnCollisionEventHandler(cam, a);
        }
    }

    if (m_pSelectedCamera)
        m_pSelectedCamera->update(deltaTimeS);

    vec3 pos = m_pSelectedCamera->getPosition();
    vec3 dir = m_pSelectedCamera->getDirection();
    m_pSoundEngine->setListenerPosition(vec3df(pos.x, pos.y, pos.z),
                                        vec3df(dir.x, dir.y, dir.z));

    if (m_pSkybox)
        m_pSkybox->setPosition(m_pSelectedCamera->getPosition());
}

gCamera *gSceneManager::addFPSCamera(vec3 pos, vec3 rotation) {
    gCamera *camera = new gFPSCamera(pos);
    m_vCameraContainer.push_back(camera);

    return camera;
}

gCamera *gSceneManager::addStaticCamera(const vec3 &pos, const vec3 &rotation) {
    gStaticCamera *camera = new gStaticCamera(pos, rotation);
    m_vCameraContainer.push_back(camera);

    return camera;
}

gCamera *gSceneManager::addSpectatorCamera(const vec3 &pos,
                                           const vec3 &rotation) {
    gCamera *camera = new gSpectatorCamera(pos);
    m_vCameraContainer.push_back(camera);

    return camera;
}

gLight *gSceneManager::addLightSource(const vec3 &position,
                                      const vec3 &rotation, E_LIGHT_TYPE type) {
    gLight *light = new gLight(position, rotation, type);
    light->setShader(
            m_pResourceMngr->loadShader("light.vs", "light.frag", "light"));
    light->setModel(m_pResourceMngr->load3dModel("cube.obj", "cube"));

    m_vLightContainer.push_back(light);

    for (int i = 0; i < m_vSceneContainer.size(); i++)
        m_vSceneContainer.at(i)->addLight(light);

    return light;
}

void gSceneManager::addRigidBody(gRigidBody *body) {
    m_vSceneContainer.push_back(body);
}

gRigidBody *gSceneManager::addRigidBody(const glm::vec3 &position,
                                        const glm::vec3 &size,
                                        const glm::vec3 &aabbSize,
                                        int mass) {
    gAABB *aabb = new gAABB(position, aabbSize);
    gRigidBody *object = new gRigidBody(aabb);
    object->setSize(size);
    object->setPosition(position);
    object->setProjection(m_mProjection);
    object->setMass(mass);

//	btCollisionShape* shape;
//	if (isStatic)
//		shape = new btStaticPlaneShape(btVector3(), 1);
//	else
//		shape = new btSphereShape(1);
//
//	btDefaultMotionState* motionState = new btDefaultMotionState();
//	btRigidBody::btRigidBodyConstructionInfo* rigidBodyCI;
//	if (isStatic)
//		rigidBodyCI = new btRigidBody::btRigidBodyConstructionInfo(0,
//				motionState, shape, btVector3(0, 0, 0));
//	else {
//		btScalar mass = 1;
//		btVector3 fallInertia(0, 0, 0);
//		shape->calculateLocalInertia(mass, fallInertia);
//		rigidBodyCI = new btRigidBody::btRigidBodyConstructionInfo(mass,
//				motionState, shape, fallInertia);
//	}
//
//	btRigidBody* body = new btRigidBody(rigidBodyCI);
//	m_pPhysicsWorld->addRigidBody(body);
//
//	object->setPhysicsBody(body);
//	object->setPhysicsWorld(m_pPhysicsWorld);

    m_vSceneContainer.push_back(object);
    return object;
}

gSprite *gSceneManager::addSprite(const vec3 &position, const vec2 &size) {
    gSprite *sprite = new gSprite(position, size);
    sprite->setProjection(m_mProjection);

    m_vSpriteContainer.push_back(sprite);
    return sprite;
}

gSkybox *gSceneManager::createSkybox(const string &path, const string &ext) {
    vector<string> textures;
    textures.push_back(string(("data/Skyboxes/" + path) + "right" + ext));
    textures.push_back(string(("data/Skyboxes/" + path) + "left" + ext));
    textures.push_back(string(("data/Skyboxes/" + path) + "bottom" + ext));
    textures.push_back(string(("data/Skyboxes/" + path) + "top" + ext));
    textures.push_back(string(("data/Skyboxes/" + path) + "back" + ext));
    textures.push_back(string(("data/Skyboxes/" + path) + "front" + ext));
    m_pSkybox = new gSkybox(textures);
    m_pSkybox->setProjection(m_mProjection);

    glActiveTexture(GL_TEXTURE0);

    glBindTexture(GL_TEXTURE_CUBE_MAP, m_pSkybox->getCubeMap());
    for (int i = 0; i < textures.size(); i++) {
        int width, height;
        unsigned char *data = m_pResourceMngr->loadRawTexture(textures.at(i),
                                                              width, height, false);
        glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB, width,
                     height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
    }
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
    glBindTexture(GL_TEXTURE_CUBE_MAP, 0);

    m_pSkybox->setShader(
            m_pResourceMngr->loadShader("sky.vs", "sky.frag", "skybox"));
    m_pSkybox->setModel(m_pResourceMngr->load3dModel("cube.obj", "cube"));

    return m_pSkybox;
}

gSound *gSceneManager::createSound(const string &path, bool loop,
                                   E_SOUND_TYPE type) {
    gSound *sound = new gSound(m_pSoundEngine, path, loop);
    sound->setType(type);
    sound->setSoundEngine(m_pSoundEngine);

    m_vSoundContainer.push_back(sound);
    return sound;
}

gARotationAnimator *gSceneManager::createRotationAnimator(const vec3 &direction,
                                                          const vec3 &speed) {
    gARotationAnimator *anim = new gARotationAnimator(direction);
    anim->setSpeed(speed);

    return anim;
}

gPathAnimator *gSceneManager::createPathAnimator(E_PATH_TYPE type, float step) {
    gPathAnimator *anim = new gPathAnimator(type, step);
    return anim;
}

gCamera *gSceneManager::selectCamera(int i) {
    if (i > m_vCameraContainer.size() - 1)
        return NULL;

    m_pSelectedCamera = m_vCameraContainer.at(i);
    return m_pSelectedCamera;
}

void gSceneManager::handleKeyPres(int key, int code, int action, int mods) {
    if (action == GLFW_PRESS) {
        switch (key) {
            case GLFW_KEY_ESCAPE:
                glfwSetWindowShouldClose(m_pWindow, GL_TRUE);
                break;
            case GLFW_KEY_W:
                m_pSelectedCamera->ProcessKeyboard(FORWARD);
                break;
            case GLFW_KEY_S:
                m_pSelectedCamera->ProcessKeyboard(BACKWARD);
                break;
            case GLFW_KEY_A:
                m_pSelectedCamera->ProcessKeyboard(LEFT);
                break;
            case GLFW_KEY_D:
                m_pSelectedCamera->ProcessKeyboard(RIGHT);
                break;
            case GLFW_KEY_SPACE:
                m_pSelectedCamera->ProcessKeyboard(JUMP);
                break;
        }
    } else if (action == GLFW_RELEASE) {
        switch (key) {
            case GLFW_KEY_W:
                m_pSelectedCamera->ProcessKeyboard(FORWARD, false);
                break;
            case GLFW_KEY_S:
                m_pSelectedCamera->ProcessKeyboard(BACKWARD, false);
                break;
            case GLFW_KEY_A:
                m_pSelectedCamera->ProcessKeyboard(LEFT, false);
                break;
            case GLFW_KEY_D:
                m_pSelectedCamera->ProcessKeyboard(RIGHT, false);
                break;
        }
    }

    if (m_fnUserHandleKeyPress)
        m_fnUserHandleKeyPress(key, code, action, mods);
}

void gSceneManager::handleMousePress(int key, int action, int mods) {
    if (m_fnUserHandleMousePress)
        m_fnUserHandleMousePress(key, action, mods);
}

void gSceneManager::handleMouseMove(double x, double y) {
    double d_x = x - m_fMouse_x;
    double d_y = m_fMouse_y - y;

    m_pSelectedCamera->ProcessMouseMovement(d_x, d_y);

    m_fMouse_x = x;
    m_fMouse_y = y;
}

void gSceneManager::setKeyPressHandler(
        void (*fn)(int key, int code, int action, int mods)) {
    m_fnUserHandleKeyPress = fn;
}

void gSceneManager::setMousePressHandler(
        void (*fn)(int key, int action, int mods)) {
    m_fnUserHandleMousePress = fn;
}

void gSceneManager::setCollisionEventHandler(
        void (*fn)(gRigidBody *a, gRigidBody *b)) {
    m_fnCollisionEventHandler = fn;
}

void gSceneManager::setMouseMoveHandler(void (*fn)(double x, double y)) {
    m_fnUserHandleMouseMove = fn;
}

void gSceneManager::setupRC() {
    m_mProjection = perspective(45.0f, (float) m_iWidth / (float) m_iHeight, 0.1f, 1000.0f);

// Setup initial GL State
    glClearColor(0.7f, 0.7f, 0.7f, 1.0f);
    glClearDepth(1.0f);

    glEnable(GL_DEPTH_TEST);    // Enables Depth Testing
    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);
    glEnable(GL_MULTISAMPLE);
//	glEnable(GL_FRAMEBUFFER_SRGB);

    // initialize framebuffer and texture
    glGenFramebuffers(1, &m_iMainFBO);
    glBindFramebuffer(GL_FRAMEBUFFER, m_iMainFBO);

    GLuint colorBuffers[2];
    glGenTextures(2, colorBuffers);

    for (int i = 0; i < 2; i++) {
        glBindTexture(GL_TEXTURE_2D, colorBuffers[i]);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, m_iWidth, m_iHeight, 0, GL_RGB, GL_FLOAT, NULL);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glBindTexture(GL_TEXTURE_2D, 0);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + i, GL_TEXTURE_2D, colorBuffers[i], 0);
    }
    m_iBloomTex = colorBuffers[1];
//	GLuint attachments[2] = {GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1};
//	glDrawBuffers(2, attachments);

    GLuint rbo;
    glGenRenderbuffers(1, &rbo);
    glBindRenderbuffer(GL_RENDERBUFFER, rbo);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, m_iWidth, m_iHeight);
    glBindRenderbuffer(GL_RENDERBUFFER, 0);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, rbo);

    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE)
        cout << "Framebuffer created!" << endl;
    else
        cerr << "Framebuffer not created!" << endl;

    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    m_pFrameBufferSprite = new gSprite(vec3(0, 0, 1), vec2(m_iWidth, m_iHeight));
    m_pFrameBufferSprite->setShader(m_pResourceMngr->loadShader("sprite.vs", "sprite.frag", "sprite"));
    m_pFrameBufferSprite->setTexture(colorBuffers[0]);
    mat4 proj = glm::ortho(
            (float) -m_iWidth / 2, (float) m_iWidth / 2,
            (float) -m_iHeight / 2, (float) m_iHeight / 2,
            -1.0f, 1.0f);
    m_pFrameBufferSprite->setProjection(proj);

    m_oShadow = m_pResourceMngr->loadShader("shadow.vs", "shadow.frag", "shadowMap");

    glGenFramebuffers(1, &m_iDepthFBO);

    glGenTextures(1, &m_iDepthMap);
    glBindTexture(GL_TEXTURE_2D, m_iDepthMap);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, m_iWidth, m_iHeight, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    glBindFramebuffer(GL_FRAMEBUFFER, m_iDepthFBO);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, m_iDepthMap, 0);
//	glDrawBuffer(GL_NONE);
//	glReadBuffer(GL_NONE);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    m_pShadowSprite = new gSprite(vec3(430, 165, -230), vec2(600, 600));
    m_pShadowSprite->setShader(m_pResourceMngr->loadShader("sprite.vs", "shadowMap.frag", "shadows"));
    m_pShadowSprite->setProjection(m_mProjection);
    m_pShadowSprite->enableBlending();
    m_pShadowSprite->setRotation(M_PI_2, 0, M_PI_2);
}
