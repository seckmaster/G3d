#include "gSound.h"

gSound::gSound(irrklang::ISoundEngine *engine, const std::string &path,
               bool loop) {
    m_pEngine = engine;
    m_sFile = path;
    m_bLoop = loop;
    m_pEngine = NULL;
    m_eType = EST_AMBIENT;
    m_pSnd = NULL;
}

gSound::gSound(irrklang::ISoundEngine *engine, const glm::vec3 &position,
               const std::string &path, bool loop) {
    m_pEngine = engine;
    setPosition(position);
    m_sFile = path;
    m_bLoop = loop;
    m_pEngine = engine;
    m_eType = EST_POSITIONAL;
    m_pSnd = NULL;
}

void gSound::play() {
    if (!m_pEngine) return;
    if (m_eType == EST_AMBIENT)
        m_pEngine->play2D(m_sFile.c_str(), m_bLoop);
    else
        m_pEngine->play3D(m_sFile.c_str(),
                          irrklang::vec3df(getPosition().x, getPosition().y, getPosition().z),
                          m_bLoop);
}

void gSound::stop() {
    if (m_pSnd) {
        m_pSnd->stop();
        m_pSnd = NULL;
    }
}
