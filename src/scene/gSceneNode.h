#ifndef SRC_SCENE_GSCENENODE_H_
#define SRC_SCENE_GSCENENODE_H_

#include "model/gModel3D.h"
#include "gActor.h"
#include <vector>

class gLight;

class gAnimator;

class gSceneNode : public gActor {
public:
    gSceneNode();

    virtual ~gSceneNode();

    // render using default shader
    virtual void render() const = 0;

    // render using different shader
    virtual void render(gShader *shader) const = 0;

    // update node
    virtual void update(float deltaTimeMS) = 0;

    inline void addLight(gLight *light) {
        m_vLightSources.push_back(light);
    }

    inline void addAnimator(gAnimator *anim) {
        m_vAnimators.push_back(anim);
    }

    inline void setShader(gShader *shader) {
        m_oShader = shader;
    }

    inline gShader *getShader() {
        return m_oShader;
    }

    inline void setProjection(const glm::mat4 &projection) {
        m_mProjection = projection;
    }

    inline void setView(const glm::mat4 &view) {
        m_mView = view;
    }

    inline const glm::mat4 &getProjection() {
        return m_mProjection;
    }

    inline const glm::mat4 &getView() {
        return m_mView;
    }

    inline void setCameraPos(const glm::vec3 &vec) {
        m_vCameraPos = vec;
    }

    inline void setModel(gModel3D *model) {
        m_pModel = model;
    }

    inline void setDiffuseTexture(GLuint tex) {
        m_iDiffuseTexture = tex;
    }

    inline void setSpecularTexture(GLuint tex) {
        m_iSpecularTexture = tex;
    }

    inline void setNormalMap(GLuint tex) {
        m_iNormalTexture = tex;
    }

    inline void setParallaxMap(GLuint tex) {
        m_iDisplacmentTexture = tex;
    }

    inline bool isVisible() {
        return m_bVisible;
    }

    inline void setVisible(bool p) {
        m_bVisible = p;
    }

    inline void setDrawable(bool p) {
        m_bDrawable = p;
    }

    inline bool isDrawable() {
        return m_bDrawable;
    }

    inline void switchVisibility() {
        m_bVisible = !m_bVisible;
    }

    inline void enableBlending() {
        m_bBlending = true;
    }

    inline void setOpacity(float opacity) {
        m_fOpacity = opacity;
    }

    inline float getOpacity() {
        return m_fOpacity;
    }

    inline void addChild(gSceneNode *child) {
        m_vChildren.push_back(child);
    }

protected:
    GLuint m_iDiffuseTexture;
    GLuint m_iSpecularTexture;
    GLuint m_iNormalTexture;
    GLuint m_iDisplacmentTexture;

    gShader *m_oShader;
    gModel3D *m_pModel;

    bool m_bVisible;
    bool m_bDrawable;
    bool m_bBlending;
    float m_fOpacity;

    // matrices
    glm::mat4 m_mProjection;
    glm::mat4 m_mView;
    // camera position
    glm::vec3 m_vCameraPos;

    std::vector<gLight*> m_vLightSources;
    std::vector<gAnimator*> m_vAnimators;

protected:
    // scene-graph
    std::vector<gSceneNode *> m_vChildren;
};

#endif /* SRC_SCENE_GSCENENODE_H_ */
