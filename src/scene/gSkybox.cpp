#include "gSkybox.h"


gSkybox::gSkybox(const std::vector<std::string> &textures) {
    glGenTextures(1, &m_iCubeMap);

}

void gSkybox::render() const {
    render(m_oShader);
}

void gSkybox::render(gShader *shader) const {
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_CULL_FACE);
//	glDisable(GL_FRAMEBUFFER_SRGB);
    glDepthMask(GL_FALSE);

    glm::mat4 model = m_oTranformation.generateModelMatrix();

    m_oShader->useProgram();
    m_oShader->setMatrix4f("model", model);
    m_oShader->setMatrix4f("view", m_mView);
    m_oShader->setMatrix4f("projection", m_mProjection);
    m_oShader->setVector3f("viewPos", m_vCameraPos);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_CUBE_MAP, m_iCubeMap);
    if (m_pModel)
        m_pModel->Draw(*shader);

    glDepthMask(GL_TRUE);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
//	glEnable(GL_FRAMEBUFFER_SRGB);
}

void gSkybox::update(float deltaTimeMS) {

}
