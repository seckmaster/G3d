//#include "gText.h"
//#include <ft2build.h>
//#include <freetype.h>
//
//gText::gText(const glm::vec3& position, int px, const std::string& font,
//		const std::string& text) :
//		gANode() {
//	m_vPosition = position;
//	m_vColor = glm::vec3(1.0, 1.0, 1.0);
//
//	FT_Library ft;
//	if (FT_Init_FreeType(&ft)) {
//		std::cerr << "Could not init freetype library\n" << std::endl;
//		return;
//	}
//	FT_Face face;
//	if (FT_New_Face(ft, font.c_str(), 0, face)) {
//		std::cerr << "Could not open font\n" << std::endl;
//		return;
//	}
//	FT_Set_Pixel_Sizes(face, 0, px);
//	m_iFontSize = px;
//
//	setShader(gShader("data/Shaders/text.vs", "data/Shaders/text.frag"));
//
//	for (GLubyte c = 0; c < 128; c++) { // lol see what I did there
//		// Load character glyph
//		if (FT_Load_Char(face, c, FT_LOAD_RENDER)) {
//			std::cout << "ERROR::FREETYTPE: Failed to load Glyph" << std::endl;
//			continue;
//		}
//		// Generate texture
//		GLuint texture;
//		glGenTextures(1, &texture);
//		glBindTexture(GL_TEXTURE_2D, texture);
//		glTexImage2D(
//		GL_TEXTURE_2D, 0,
//		GL_RED, face->glyph->bitmap.width, face->glyph->bitmap.rows, 0,
//		GL_RED,
//		GL_UNSIGNED_BYTE, face->glyph->bitmap);
//		// Set texture options
//		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
//		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
//		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
//		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
//
//		// Now store character for later use
//		Character character = { texture, glm::ivec2(face->glyph->bitmap.width,
//				face->glyph->bitmap.rows), glm::ivec2(face->glyph->bitmap_left,
//				face->glyph->bitmap_top), face->glyph->advance.x };
//		Characters.insert(std::pair<GLchar, Character>(c, character));
//	}
//	glBindTexture(GL_TEXTURE_2D, 0);
//
//	glGenVertexArrays(1, &m_iVAO);
//	glGenBuffers(1, &this->m_iVBO);
//	glBindVertexArray(this->m_iVAO);
//	glBindBuffer(GL_ARRAY_BUFFER, this->m_iVBO);
//	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 6 * 4, NULL,
//	GL_DYNAMIC_DRAW);
//	glEnableVertexAttribArray(0);
//	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), 0);
//	glBindBuffer(GL_ARRAY_BUFFER, 0);
//	glBindVertexArray(0);
//}
//
//void gText::render() const {
//	glDisable(GL_CULL_FACE);
//	glUseProgram(m_oShader.Program);
//
//	glm::mat4 model;
//	model = glm::translate(model, m_vPosition);
//	model = glm::scale(model, glm::vec3(m_vSize.x, m_vSize.y, 1.0f));
//
//	GLuint loc = glGetUniformLocation(m_oShader.Program, "model");
//	glUniformMatrix4fv(loc, 1, GL_FALSE, glm::value_ptr(model));
//	loc = glGetUniformLocation(m_oShader.Program, "view");
//	glUniformMatrix4fv(loc, 1, GL_FALSE, glm::value_ptr(m_mView));
//	loc = glGetUniformLocation(m_oShader.Program, "projection");
//	glUniformMatrix4fv(loc, 1, GL_FALSE, glm::value_ptr(m_mProjection));
//
//	loc = glGetUniformLocation(m_oShader.Program, "spriteColor");
//	glUniform3fv(loc, 1, glm::value_ptr(m_vColor));
//
//	glActiveTexture(GL_TEXTURE0);
//
//	glBindVertexArray(m_iVAO);
//
//	// Iterate through all characters
//	float x = m_vPosition.x;
//	std::string::const_iterator c;
//	for (c = m_sText.begin(); c != m_sText.end(); c++) {
//		Character ch = Characters.at(*c);
//
//		GLfloat xpos = x + ch.bearing.x * 50;
//		GLfloat ypos = m_vPosition.y
//				+ (this->Characters.at('H').bearing.y - ch.bearing.y) * 50;
//
//		GLfloat w = ch.size.x * 50;
//		GLfloat h = ch.size.y * 50;
//		// Update VBO for each character
//		GLfloat vertices[6][4] = { { xpos, ypos + h, 0.0, 1.0 }, { xpos + w,
//				ypos, 1.0, 0.0 }, { xpos, ypos, 0.0, 0.0 }, { xpos, ypos + h,
//				0.0, 1.0 }, { xpos + w, ypos + h, 1.0, 1.0 }, { xpos + w, ypos,
//				1.0, 0.0 } };
//		// Render glyph texture over quad
//		glBindTexture(GL_TEXTURE_2D, ch.texture);
//		// Update content of VBO memory
//		glBindBuffer(GL_ARRAY_BUFFER, this->m_iVBO);
//		glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices); // Be sure to use glBufferSubData and not glBufferData
//
//		glBindBuffer(GL_ARRAY_BUFFER, 0);
//		// Render quad
//		glDrawArrays(GL_TRIANGLES, 0, 6);
//		// Now advance cursors for next glyph
//		x += (ch.next >> 6) * 50; // Bitshift by 6 to get value in pixels (1/64th times 2^6 = 64)
//	}
//	glBindVertexArray(0);
//	glBindTexture(GL_TEXTURE_2D, 0);
//
//	glEnable(GL_CULL_FACE);
//}
//
//void gText::update(float deltaTimeMS) {
//
//}
