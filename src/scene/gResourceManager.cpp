
#include "gResourceManager.h"

#include <OpenGL/gl3.h>
#include <IL/il.h>
#include <IL/ilut.h>

#include <fstream>
#include <sstream>

using namespace std;

gResourceManager::gResourceManager() {
}

GLuint gResourceManager::loadTexture(const string &path, const string &name,
                                     bool prefix) {
    glEnable(GL_TEXTURE_2D);

    /**
     * Check if texture is already loaded.
     */
    for (map<string, GLuint>::iterator i = m_dictTextures.begin();
         i != m_dictTextures.end(); ++i) {
        if ((*i).first == name)
            return (*i).second;
    }

    GLuint id;
    if (prefix)
        id = loadGLTexture(("data/Textures/" + path).c_str());
    else
        id = loadGLTexture(path.c_str());

    m_dictTextures[name] = id;
    return id;
}

unsigned char *gResourceManager::loadRawTexture(const std::string &path,
                                                int &width, int &height, bool prefix) {
    ILboolean success;
    if (prefix)
        success = ilLoadImage(("data/Textures/" + path).c_str());
    else
        success = ilLoadImage(path.c_str());

    if (success) {
        // If the image is flipped (i.e. upside-down and mirrored, flip it the right way up!)
        ILinfo ImageInfo;
        iluGetImageInfo(&ImageInfo);
        if (ImageInfo.Origin == IL_ORIGIN_UPPER_LEFT) {
            iluFlipImage();
        }

        // Convert the image into a suitable format to work with
        // NOTE: If your image contains alpha channel you can replace IL_RGB with IL_RGBA
        success = ilConvertImage(IL_RGB, IL_UNSIGNED_BYTE);

        // Quit out if we failed the conversion
        if (!success) {
            ILenum error = ilGetError();
            std::cout << "Image conversion failed - IL reports error: " << error
                      << " - " << iluErrorString(error) << std::endl;
            return NULL;
        }

        width = ilGetInteger(IL_IMAGE_WIDTH);
        height = ilGetInteger(IL_IMAGE_HEIGHT);

        return ilGetData();
    }
    return NULL;
}

gShader *gResourceManager::loadShader(const string &vertexPath,
                                      const string &fragmentPath,
                                      const string &name) {
    /**
     * Check if shader already exists.
     */
    for (map<string, gShader *>::iterator i = m_dictShaders.begin(); i != m_dictShaders.end(); ++i) {
        if ((*i).first == name) {
            return (*i).second;
        }
    }

    // 1. Retrieve the vertex/fragment source code from filePath
    std::string vertexCode;
    std::string fragmentCode;
    std::ifstream vShaderFile;
    std::ifstream fShaderFile;

    // ensures ifstream objects can throw exceptions:
    vShaderFile.exceptions(std::ifstream::badbit);
    fShaderFile.exceptions(std::ifstream::badbit);

    std::stringstream vShaderStream, fShaderStream;

    GLuint vertex = -1, fragment = -1;
    GLint success;
    GLchar infoLog[512];

    // Vertex Shader
    try {
        vShaderFile.open(("data/Shaders/" + vertexPath));
        vShaderStream << vShaderFile.rdbuf();
        vShaderFile.close();
        vertexCode = vShaderStream.str();
    } catch (std::ifstream::failure e) {
        std::cerr << "Error reading vertex file!" << std::endl;
        return NULL;
    }
    const GLchar *vShaderCode = vertexCode.c_str();

    vertex = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertex, 1, &vShaderCode, NULL);
    glCompileShader(vertex);
    // Print compile errors if any
    glGetShaderiv(vertex, GL_COMPILE_STATUS, &success);
    if (!success) {
        glGetShaderInfoLog(vertex, 512, NULL, infoLog);
        std::cerr << "Error! Vertex compilation failed:\n" << infoLog
                  << std::endl;
        return NULL;
    };

    // Similiar for Fragment Shader
    try {
        fShaderFile.open(("data/Shaders/" + fragmentPath));
        fShaderStream << fShaderFile.rdbuf();
        fShaderFile.close();
        fragmentCode = fShaderStream.str();
        // close file handlers
        // Convert stream into GLchar array
    } catch (std::ifstream::failure e) {
        std::cerr << "Error reading fragment file!" << std::endl;
        return NULL;
    }
    const GLchar *fShaderCode = fragmentCode.c_str();

    fragment = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragment, 1, &fShaderCode, NULL);
    glCompileShader(fragment);
    // Print compile errors if any
    glGetShaderiv(fragment, GL_COMPILE_STATUS, &success);
    if (!success) {
        glGetShaderInfoLog(vertex, 512, NULL, infoLog);
        std::cerr << "Error! Fragment compilation failed:\n" << infoLog << ", "
                  << fragmentPath << std::endl;
        return NULL;
    };

    // Shader Program
    gShader* shader = new gShader(name, glCreateProgram());
    glAttachShader(shader->getProgramId(), vertex);
    glAttachShader(shader->getProgramId(), fragment);
    glLinkProgram(shader->getProgramId());

    // Print linking errors if any
    glGetProgramiv(shader->getProgramId(), GL_LINK_STATUS, &success);
    if (!success) {
        glGetProgramInfoLog(shader->getProgramId(), 512, NULL, infoLog);
        std::cerr << "Error! Linking failed:\n" << infoLog << ", " << vertexPath
                  << std::endl;
        return NULL;
    }

    std::cout << "Shader: (" << vertexPath << ", " << fragmentPath
              << "), "<< shader->getProgramId() << " successfuly loaded!" << std::endl;

    // Delete the shaders as they're linked into our program now and no longer necessary
    glDeleteShader(vertex);
    glDeleteShader(fragment);

    m_dictShaders[name] = shader;
    return shader;
}

gModel3D *gResourceManager::load3dModel(const string &path, const string &name,
                                        int params) {
    /**
     * Check if model already exists.
     */
    for (map<string, gModel3D *>::iterator i = m_dictModels.begin();
         i != m_dictModels.end(); ++i) {
        if ((*i).first == name)
            return (*i).second;
    }

    gModel3D *model = new gModel3D();

    // Read file via ASSIMP
    Assimp::Importer importer;
    const aiScene *scene = importer.ReadFile(("data/Models/" + path),
                                             aiProcess_Triangulate | aiProcess_FlipUVs | aiProcess_GenNormals
                                             | params);
    // Check for errors
    if (!scene || scene->mFlags == AI_SCENE_FLAGS_INCOMPLETE
        || !scene->mRootNode) // if is Not Zero
    {
        std::cout << "ERROR::ASSIMP:: " << importer.GetErrorString()
                  << std::endl;
        return NULL;
    }
    // Retrieve the directory path of the filepath
    model->directory = "data/Models/" + path.substr(0, path.find_last_of('/'));

    // Process ASSIMP's root node recursively
    processNode(model, scene->mRootNode, scene);

    m_dictModels[name] = model;
    return model;
}

GLuint gResourceManager::loadGLTexture(const std::string &path) {
    ILuint imageID;        // Create an image ID as a ULuint
    GLuint textureID;      // Create a texture ID as a GLuint
    ILboolean success;    // Create a flag to keep track of success/failure
    ILenum error;      // Create a flag to keep track of the IL error state
    ilGenImages(1, &imageID);    // Generate the image ID
    ilBindImage(imageID);      // Bind the image
    success = ilLoadImage(path.c_str());  // Load the image file

    // If we managed to load the image, then we can start to do things with it...
    if (success) {
        // If the image is flipped (i.e. upside-down and mirrored, flip it the right way up!)
        ILinfo ImageInfo;
        iluGetImageInfo(&ImageInfo);
        if (ImageInfo.Origin == IL_ORIGIN_UPPER_LEFT) {
            iluFlipImage();
        }

        // Convert the image into a suitable format to work with
        // NOTE: If your image contains alpha channel you can replace IL_RGB with IL_RGBA
        success = ilConvertImage(IL_RGB, IL_UNSIGNED_BYTE);

        // Quit out if we failed the conversion
        if (!success) {
            error = ilGetError();
            std::cout << "Image conversion failed - IL reports error: " << error
                      << " - " << iluErrorString(error) << std::endl;
            return -1;
        }

        // Generate a new texture
        glGenTextures(1, &textureID);

        // Bind the texture to a name
        glBindTexture(GL_TEXTURE_2D, textureID);

        // Set texture interpolation method to use linear interpolation (no MIPMAPS)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

        // Specify the texture specification
        glTexImage2D(GL_TEXTURE_2D,        // Type of texture
                     0,    // Pyramid level (for mip-mapping) - 0 is the top level
                     ilGetInteger(
                             IL_IMAGE_FORMAT),// Internal pixel format to use. Can be a generic type like GL_RGB or GL_RGBA, or a sized type
                     ilGetInteger(IL_IMAGE_WIDTH),  // Image width
                     ilGetInteger(IL_IMAGE_HEIGHT),  // Image height
                     0,        // Border width in pixels (can either be 1 or 0)
                     ilGetInteger(IL_IMAGE_FORMAT),  // Format of image pixel data
                     GL_UNSIGNED_BYTE,    // Image data type
                     ilGetData());      // The actual image data itself

        std::cout << "Image successfully loaded: " << path << endl;
    } else // If we failed to open the image file in the first place...
    {
        error = ilGetError();
        std::cout << "Image load failed - IL reports error: " << error << " - "
                  << iluErrorString(error) << " File: " << path << std::endl;
        return -1;
    }

    ilDeleteImages(1,
                   &imageID); // Because we have already copied image data into texture data we can release memory used by image.

    std::cout << "Texture: " << path << " successfuly loaded!" << std::endl;

    return textureID; // Return the GLuint to the texture so you can use it!
}

void gResourceManager::processNode(gModel3D *model, aiNode *node,
                                   const aiScene *scene) {
    // Process each mesh located at the current node
    for (GLuint i = 0; i < node->mNumMeshes; i++) {
        // The node object only contains indices to index the actual objects in the scene.
        // The scene contains all the data, node is just to keep stuff organized (like relations between nodes).
        aiMesh *mesh = scene->mMeshes[node->mMeshes[i]];
        model->meshes.push_back(this->processMesh(model, mesh, scene));
    }
    // After we've processed all of the meshes (if any) we then recursively process each of the children nodes
    for (GLuint i = 0; i < node->mNumChildren; i++) {
        this->processNode(model, node->mChildren[i], scene);
    }
}

gMesh gResourceManager::processMesh(gModel3D *model, aiMesh *mesh,
                                    const aiScene *scene) {
    std::vector<Vertex> vertices;
    std::vector<GLuint> indices;
    std::vector<Texture> textures;

    // Walk through each of the mesh's vertices
    for (GLuint i = 0; i < mesh->mNumVertices; i++) {
        Vertex vertex;
        glm::vec3 vector; // We declare a placeholder vector since assimp uses its own vector class that doesn't directly convert to glm's vec3 class so we transfer the data to this placeholder glm::vec3 first.
        // Positions
        vector.x = mesh->mVertices[i].x;
        vector.y = mesh->mVertices[i].y;
        vector.z = mesh->mVertices[i].z;
        vertex.Position = vector;
        // Normals
        vector.x = mesh->mNormals[i].x;
        vector.y = mesh->mNormals[i].y;
        vector.z = mesh->mNormals[i].z;
        vertex.Normal = vector;
        // Tangents
        if (mesh->HasTangentsAndBitangents()) {
            vector.x = mesh->mTangents[i].x;
            vector.y = mesh->mTangents[i].y;
            vector.z = mesh->mTangents[i].z;
            vertex.Tangents = vector;
//			std::cout << "tangent: " << glm::to_string(vector) << std::endl;
            vector.x = mesh->mBitangents[i].x;
            vector.y = mesh->mBitangents[i].y;
            vector.z = mesh->mBitangents[i].z;
            vertex.biTangents = vector;
//			std::cout << "bitangent: " << glm::to_string(vector) << std::endl;
        }
        // Texture Coordinates
        if (mesh->mTextureCoords[0]) // Does the mesh contain texture coordinates?
        {
            glm::vec2 vec;
            // A vertex can contain up to 8 different texture coordinates. We thus make the assumption that we won't
            // use models where a vertex can have multiple texture coordinates so we always take the first set (0).
            vec.x = mesh->mTextureCoords[0][i].x;
            vec.y = mesh->mTextureCoords[0][i].y;
            vertex.TexCoords = vec;
        } else
            vertex.TexCoords = glm::vec2(0.0f, 0.0f);
        vertices.push_back(vertex);
    }
    // Now wak through each of the mesh's faces (a face is a mesh its triangle) and retrieve the corresponding vertex indices.
    for (GLuint i = 0; i < mesh->mNumFaces; i++) {
        aiFace face = mesh->mFaces[i];
        // Retrieve all indices of the face and store them in the indices vector
        for (GLuint j = 0; j < face.mNumIndices; j++)
            indices.push_back(face.mIndices[j]);
    }
    // Process materials
    aiMaterial *material = scene->mMaterials[mesh->mMaterialIndex];
    // We assume a convention for sampler names in the shaders. Each diffuse texture should be named
    // as 'texture_diffuseN' where N is a sequential number ranging from 1 to MAX_SAMPLER_NUMBER.
    // Same applies to other texture as the following list summarizes:
    // Diffuse: texture_diffuseN
    // Specular: texture_specularN
    // Normal: texture_normalN

    // 1. Diffuse maps
    std::vector<Texture> diffuseMaps = this->loadMaterialTextures(
            model->directory, material, aiTextureType_DIFFUSE,
            "texture_diffuse");
    textures.insert(textures.end(), diffuseMaps.begin(), diffuseMaps.end());
    // 2. Specular maps
    std::vector<Texture> specularMaps = this->loadMaterialTextures(
            model->directory, material, aiTextureType_SPECULAR,
            "texture_specular");
    textures.insert(textures.end(), specularMaps.begin(), specularMaps.end());
    // 3. Normal maps
    std::vector<Texture> normalMaps = this->loadMaterialTextures(
            model->directory, material, aiTextureType_HEIGHT, "texture_normal");
    textures.insert(textures.end(), normalMaps.begin(), normalMaps.end());

    // Return a mesh object created from the extracted mesh data
    return gMesh(vertices, indices, textures);
}

std::vector<Texture> gResourceManager::loadMaterialTextures(
        const std::string &directory, aiMaterial *mat, aiTextureType type,
        const std::string &typeName) {
    std::vector<Texture> textures;
    for (GLuint i = 0; i < mat->GetTextureCount(type); i++) {
        aiString str;
        mat->GetTexture(type, i, &str);
        Texture texture;
        std::string tex = std::string(str.C_Str());
        tex = tex.substr(tex.find('\\') + 1, tex.size());
        texture.id = loadTexture(directory + "/" + tex, directory + "/" + tex,
                                 false);
        texture.type = typeName;
        texture.path = str.C_Str();
        textures.push_back(texture);
    }
    return textures;
}
