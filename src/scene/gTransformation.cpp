#include "gTransformation.h"

gTransformation::gTransformation() {

}

gTransformation::gTransformation(const glm::vec3 &position,
                                 const glm::vec3 &rotation, const glm::vec3 &scale) :
        m_vPosition(position), m_vRotation(rotation), m_vScale(scale) {
}

gTransformation::gTransformation(const gTransformation &o) {
    this->m_vPosition = o.m_vPosition;
    this->m_vRotation = o.m_vRotation;
    this->m_vScale = o.m_vScale;
}

glm::mat4 gTransformation::generateModelMatrix() const {
    glm::mat4 model;
    model = glm::translate(model, m_vPosition);
    model = glm::rotate(model, m_vRotation.x, glm::vec3(1.0f, 0.0f, 0.0f));
    model = glm::rotate(model, m_vRotation.y, glm::vec3(0.0f, 1.0f, 0.0f));
    model = glm::rotate(model, m_vRotation.z, glm::vec3(0.0f, 0.0f, 1.0f));
    model = glm::scale(model, m_vScale);

    return model;
}
