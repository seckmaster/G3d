
#include "gARotationAnimator.h"
#include "../gActor.h"

void gARotationAnimator::animateNode(gActor *node, float deltaTimeMS) {
    if (!m_bStop) {
        glm::vec3 angle = node->getRotation();
        node->setRotation(angle + (m_vDirection * (m_vSpeed * deltaTimeMS)));
    }
}
