#include "gPathAnimator.h"
#include "../gActor.h"
#include "../gSceneNode.h"

void gPathAnimator::animateNode(gActor *node, float deltaTimeMS) {
    m_fDurationS -= deltaTimeMS;
    if (m_fDurationS <= 0.0f) {
        if (m_eOnStopEvent != EASE_DO_NOTHING) {
            m_bStop = false;
            m_bLoop = false;
        }
        if (m_eOnStopEvent == EASE_STOP_ANIMATOR_AND_HIDE_OBJECT) {
            static_cast<gSceneNode *>(node)->setDrawable(false);
        }
    }

    if (m_bStop) {
        if (m_bLoop) {
            m_bStop = false;
            m_fPar = 0.0f;
        }
    }

    if (!m_bStop) {
        node->setPosition(m_oPath->calculatePosition(m_fPar));
        m_fPar += m_fStep * deltaTimeMS;
        if (m_oPath->getSize() - 1 <= (int) m_fPar)
            m_bStop = true;
    }
}
