
#include "gFadeAnimator.h"
#include "../gSprite.h"

gFadeAnimator::gFadeAnimator(float step, float end) :
        step(step), end(end) {
}

void gFadeAnimator::animateNode(gActor *node, float deltaTimeS) {
    if (!m_bStop) {
        gSprite *sprite = static_cast<gSprite *>(node);
        sprite->setOpacity(sprite->getOpacity() + step);
//		if (end >= sprite->getOpacity())
//			m_bStop = true;
    } else {
        if (m_bLoop) {
            step *= -1;
            end = 1 - end;
            m_bStop = false;
        }
    }
}
