#include "gTextureAnimator.h"
#include "../gSprite.h"

gTextureAnimator::gTextureAnimator(int nOfFrames) {
    setFPS(60);
    m_fmsPrevious = -1.0f;
    m_iNumberOfFrames = nOfFrames;
    m_iCurrentFrame = 0;
}

void gTextureAnimator::animateNode(gActor *actor, float deltaTimeS) {
    gSprite *sprite = static_cast<gSprite *>(actor);

    if (!m_bStop && sprite->isTextureAnimationEnabled()) {
        m_fSecToWait -= deltaTimeS;

        if (m_fSecToWait <= 0.0f) {
            if (!m_bStop) {
                sprite->getShader()->setFloat1f("frameNumber", m_iCurrentFrame);
                sprite->getShader()->setFloat1f("numberOfFrames", m_iNumberOfFrames);
            }

            m_iCurrentFrame++;
            if (m_iCurrentFrame == m_iNumberOfFrames)
                m_bStop = !m_bLoop;

            m_fSecToWait = m_fSecToWaitConst;
        }
    }
}

