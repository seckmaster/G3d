#ifndef SRC_SCENE_ANIMATOR_GPATHANIMATOR_H_
#define SRC_SCENE_ANIMATOR_GPATHANIMATOR_H_

#include "gAnimator.h"
#include "gPath.h"

class gPathAnimator : public gAnimator {
public:
    gPathAnimator(E_PATH_TYPE type, float step) :
            gAnimator() {
        m_oPath = new gPath(type);
        m_fStep = step;
        m_fPar = 0.0f;
    }

    inline void addPoint(const glm::vec3 &point) {
        m_oPath->addPoint(point);
    }

    void animateNode(gActor *node, float deltaTimeMS);

private:
    gPath *m_oPath;
    float m_fStep;
    float m_fPar;
};

#endif /* SRC_SCENE_ANIMATOR_GPATHANIMATOR_H_ */
