
#include "gPath.h"

glm::vec3 gPath::calculatePosition(float t) {
    if (m_eType == CURVE)
        return calculateBezier(t);

    return calculateLine(t);
}

glm::vec3 gPath::calculateLine(float t) {
    if (m_vPoints.size() < 2)
        return glm::vec3();

    double tmp;
    t = modf(t, &tmp);
    return m_vPoints[tmp] + (m_vPoints[tmp + 1] - m_vPoints[tmp]) * t;
}

glm::vec3 gPath::calculateBezier(float t) {

}

void gPath::addPoint(const glm::vec3 &point) {
    m_vPoints.push_back(point);
}
