
#ifndef SRC_SCENE_GPATH_H_
#define SRC_SCENE_GPATH_H_

#include <vector>
#include <iostream>
#include <glm/glm.hpp>
#include <math.h>

enum E_PATH_TYPE {
    LINE,
    CURVE
};

class gPath {
public:
    gPath(E_PATH_TYPE type) { m_eType = type; }

    void addPoint(const glm::vec3 &point);

    inline size_t getSize() { return m_vPoints.size(); }

    glm::vec3 calculatePosition(float t);

private:
    std::vector<glm::vec3> m_vPoints;

    E_PATH_TYPE m_eType;

    glm::vec3 calculateBezier(float t);

    glm::vec3 calculateLine(float t);
};

#endif /* SRC_SCENE_GPATH_H_ */
