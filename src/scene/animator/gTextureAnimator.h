#ifndef SRC_SCENE_ANIMATOR_GTEXTUREANIMATOR_H_
#define SRC_SCENE_ANIMATOR_GTEXTUREANIMATOR_H_

#include "gAnimator.h"

class gTextureAnimator : public gAnimator {
public:
    gTextureAnimator(int nOfFrames);

    void animateNode(gActor *actor, float deltaTimeS);

    inline void setFPS(float framesPerSecond) {
        m_fFPS = framesPerSecond;
        m_fSecToWait = (1 / m_fFPS);
        m_fSecToWaitConst = m_fSecToWait;
    }


private:
    int m_iNumberOfFrames;
    int m_iCurrentFrame;

    float m_fFPS;
    float m_fSecToWait;
    float m_fSecToWaitConst;
    float m_fmsPrevious;
};

#endif /* SRC_SCENE_ANIMATOR_GTEXTUREANIMATOR_H_ */
