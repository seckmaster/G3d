
#ifndef SRC_SCENE_ANIMATOR_GAROTATIONANIMATOR_H_
#define SRC_SCENE_ANIMATOR_GAROTATIONANIMATOR_H_

#include "gAnimator.h"

class gARotationAnimator : public gAnimator {
public:
    gARotationAnimator(const glm::vec3 &d) : gAnimator() {
        m_vDirection = d;
        m_vSpeed = glm::vec3(1, 1, 1);
    };

    void animateNode(gActor *node, float deltaTimeMS);

    void setSpeed(const glm::vec3 &speed) { m_vSpeed = speed; }

private:
    glm::vec3 m_vDirection;
    glm::vec3 m_vSpeed;
};

#endif /* SRC_SCENE_ANIMATOR_GAROTATIONANIMATOR_H_ */
