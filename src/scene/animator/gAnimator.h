#ifndef SRC_SCENE_GANIMATOR_H_
#define SRC_SCENE_GANIMATOR_H_

#include <glm/glm.hpp>

/**
 * What should happen when animator's duration expires.
 */
enum E_ANIMATOR_STOP_EVENT {
    /* 0 */EASE_STOP_ANIMATOR,
    /* 1 */EASE_STOP_ANIMATOR_AND_HIDE_OBJECT,
    /* 2 */EASE_DO_NOTHING
};

class gActor;

class gAnimator {
public:
    gAnimator();

    virtual ~gAnimator();

    virtual void animateNode(gActor *node, float deltaTimeMS) = 0;

    inline void stopAnimation() {
        m_bStop = true;
    }

    inline void playAnimation() {
        m_bStop = false;
    }

    inline void setLoop(bool l) {
        m_bLoop = l;
    }

    inline void setDuration(float sec) {
        m_fDurationS = sec;
    }

    inline void setOnStopEvent(E_ANIMATOR_STOP_EVENT event) {
        m_eOnStopEvent = event;
    }

protected:
    bool m_bLoop;
    bool m_bStop;
    float m_fDurationS;
    E_ANIMATOR_STOP_EVENT m_eOnStopEvent;

};

#endif /* SRC_SCENE_GANIMATOR_H_ */
