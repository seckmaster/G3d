#ifndef SRC_SCENE_ANIMATOR_GFADEANIMATOR_H_
#define SRC_SCENE_ANIMATOR_GFADEANIMATOR_H_

#include "gAnimator.h"

class gFadeAnimator : public gAnimator {
public:
    gFadeAnimator(float step, float end);

    void animateNode(gActor *node, float deltaTimeS);

private:
    float step;
    float end;
};

#endif /* SRC_SCENE_ANIMATOR_GFADEANIMATOR_H_ */
