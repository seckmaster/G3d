#include "gAnimator.h"

gAnimator::gAnimator() {
    m_bLoop = false;
    m_bStop = false;

    m_eOnStopEvent = EASE_DO_NOTHING;
    m_fDurationS = -1.0f;
}

gAnimator::~gAnimator() {
}
