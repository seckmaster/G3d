################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/scene/animator/gARotationAnimator.cpp \
../src/scene/animator/gAnimator.cpp \
../src/scene/animator/gFadeAnimator.cpp \
../src/scene/animator/gPath.cpp \
../src/scene/animator/gPathAnimator.cpp \
../src/scene/animator/gTextureAnimator.cpp 

OBJS += \
./src/scene/animator/gARotationAnimator.o \
./src/scene/animator/gAnimator.o \
./src/scene/animator/gFadeAnimator.o \
./src/scene/animator/gPath.o \
./src/scene/animator/gPathAnimator.o \
./src/scene/animator/gTextureAnimator.o 

CPP_DEPS += \
./src/scene/animator/gARotationAnimator.d \
./src/scene/animator/gAnimator.d \
./src/scene/animator/gFadeAnimator.d \
./src/scene/animator/gPath.d \
./src/scene/animator/gPathAnimator.d \
./src/scene/animator/gTextureAnimator.d 


# Each subdirectory must supply rules for building sources it contributes
src/scene/animator/%.o: ../src/scene/animator/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	clang++ -I/System/Library/Frameworks/OpenGL.framework/Versions/A/Headers -I"/Users/toni/Documents/workspace/G3dEngine/include" -O0 -Wall -c -fmessage-length=0 -g -O0 -std=c++11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


