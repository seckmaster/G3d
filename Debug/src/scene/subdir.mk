################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/scene/gAABB.cpp \
../src/scene/gActor.cpp \
../src/scene/gLight.cpp \
../src/scene/gMaterial.cpp \
../src/scene/gResourceManager.cpp \
../src/scene/gRigidBody.cpp \
../src/scene/gSceneManager.cpp \
../src/scene/gSceneNode.cpp \
../src/scene/gSkybox.cpp \
../src/scene/gSound.cpp \
../src/scene/gSprite.cpp \
../src/scene/gText.cpp \
../src/scene/gTransformation.cpp 

OBJS += \
./src/scene/gAABB.o \
./src/scene/gActor.o \
./src/scene/gLight.o \
./src/scene/gMaterial.o \
./src/scene/gResourceManager.o \
./src/scene/gRigidBody.o \
./src/scene/gSceneManager.o \
./src/scene/gSceneNode.o \
./src/scene/gSkybox.o \
./src/scene/gSound.o \
./src/scene/gSprite.o \
./src/scene/gText.o \
./src/scene/gTransformation.o 

CPP_DEPS += \
./src/scene/gAABB.d \
./src/scene/gActor.d \
./src/scene/gLight.d \
./src/scene/gMaterial.d \
./src/scene/gResourceManager.d \
./src/scene/gRigidBody.d \
./src/scene/gSceneManager.d \
./src/scene/gSceneNode.d \
./src/scene/gSkybox.d \
./src/scene/gSound.d \
./src/scene/gSprite.d \
./src/scene/gText.d \
./src/scene/gTransformation.d 


# Each subdirectory must supply rules for building sources it contributes
src/scene/%.o: ../src/scene/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	clang++ -I/System/Library/Frameworks/OpenGL.framework/Versions/A/Headers -I"/Users/toni/Documents/workspace/G3dEngine/include" -O0 -Wall -c -fmessage-length=0 -g -O0 -std=c++11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


