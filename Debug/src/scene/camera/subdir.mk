################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/scene/camera/gCamera.cpp \
../src/scene/camera/gFPSCamera.cpp \
../src/scene/camera/gSpectatorCamera.cpp \
../src/scene/camera/gStaticCamera.cpp 

OBJS += \
./src/scene/camera/gCamera.o \
./src/scene/camera/gFPSCamera.o \
./src/scene/camera/gSpectatorCamera.o \
./src/scene/camera/gStaticCamera.o 

CPP_DEPS += \
./src/scene/camera/gCamera.d \
./src/scene/camera/gFPSCamera.d \
./src/scene/camera/gSpectatorCamera.d \
./src/scene/camera/gStaticCamera.d 


# Each subdirectory must supply rules for building sources it contributes
src/scene/camera/%.o: ../src/scene/camera/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	clang++ -I/System/Library/Frameworks/OpenGL.framework/Versions/A/Headers -I"/Users/toni/Documents/workspace/G3dEngine/include" -O0 -Wall -c -fmessage-length=0 -g -O0 -std=c++11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


